function serializeFormObject(selector) {
    var a, o, h, i, e;
    a = $(selector).serializeArray();
    o = {};
    h = o.hasOwnProperty;
    for (i = 0; i < a.length; i++) {
        e = a[i];
        if (!h.call(o, e.name)) {
            o[e.name] = e.value;
        }
    }
    return o;
}

var localWareHouse = {
    get(key) {
        let v = localStorage[key]
        if (v&&v.startsWith('{')) {
            return JSON.parse(v)
        } else {
            return v
        }

    },
    set(key, value) {
        switch (typeof value) {
            case "object":
                localStorage.setItem(key, JSON.stringify(value));
                break
            case "undefined":
                break
            default:
                localStorage.setItem(key, value);
        }
    },
    rm(key) {
        localStorage.removeItem(key)
    },
    len() {
        return !!localStorage ? localStorage.length : 0
    },
    clear() {
        localStorage.clear()
    }
}