const context = ''
const api = {
    person: {
        get: context + '/person/get',
        getLike: context + '/person/getlike',
        add: context + '/person/add',
        del: context + '/person/del',
        upd: context + '/person/upd',
        login: context + '/person/login'
    },
    class: {
        get: context + '/class/get',
        getLike: context + '/class/getlike',
        add: context + '/class/add',
        del: context + '/class/del',
        upd: context + '/class/upd'
    },
    personType: {
        get: context + '/persontype/get',
        add: context + '/persontype/add',
        del: context + '/persontype/del'
    },
    major: {
        get: context + '/major/get',
        getLike: context + '/major/getlike',
        add: context + '/major/add',
        del: context + '/major/del',
        upd: context + '/major/upd'
    },
    ability: {
        getBilities: context + '/ability/getbilities' // 传角色id获取一个code数组
    },
    grade: {
        getGradeByPerson: context + '/grade/getgradebyperson',
        getLike: context + '/grade/getlike',
        add: context + '/grade/add',
        del: context + '/grade/del',
        upd: context + '/grade/upd'
    },
    course: {
        get: context + '/course/get',
        getLike: context + '/course/getlike',
        add: context + '/course/add',
        del: context + '/course/del',
        upd: context + '/course/upd',
        getArrangement: context + '/course/getarrangement',
        addArrangement: context + '/course/addarrangement',
        delArrangement: context + '/course/delarrangement',
        updArrangement: context + '/course/updatearrangement',
        elect: context + '/course/elect'
    },
    echarts: {
        majorData: context + "/echarts/majordata",
        majorRecruitmentData: context + '/echarts/majorrecruitmentdata',
        courseAverages: context + '/echarts/courseaverages'
    }
}