package com.todd.studentms.service;

import com.todd.studentms.entity.Person;
import com.todd.studentms.entity.vo.GradeJoinPersonJoinCourse;

import java.util.List;

public interface GradeService extends EssentialService {
    List<GradeJoinPersonJoinCourse> getGradeByPerson(Person person) throws Exception;
    List<GradeJoinPersonJoinCourse> getLike(GradeJoinPersonJoinCourse gpj,String personTypeId) throws Exception;
}
