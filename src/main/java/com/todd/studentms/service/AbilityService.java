package com.todd.studentms.service;

import com.todd.studentms.entity.Ability;

import java.util.List;

public interface AbilityService extends EssentialService {
    List<String> getBilities(String personTypeId) throws Exception;
}
