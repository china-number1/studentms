package com.todd.studentms.service;

import java.util.List;

public interface EssentialService {
    <T> List<T> get(T obj);
    <T> T getOne(T obj);
    <T> List<T> getLike(T obj);
    boolean update(Object obj);
    boolean add(Object obj);
    boolean delete(Class cls,Object[] ids);
    boolean delete(Class cls,Object id);
}
