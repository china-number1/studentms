package com.todd.studentms.service;

import com.todd.studentms.entity.CourseArrangement;
import com.todd.studentms.entity.Person;

import java.util.List;

public interface CourseService extends EssentialService {
    List<CourseArrangement> getArrangement(CourseArrangement ca,Person curPerson);
    boolean addArrangement(CourseArrangement ca);
    boolean elect(String curPersonId, String cid);
}
