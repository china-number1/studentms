package com.todd.studentms.service.impl;

import com.todd.studentms.entity.Class;
import com.todd.studentms.entity.Grade;
import com.todd.studentms.entity.Person;
import com.todd.studentms.service.ClassService;
import com.todd.studentms.service.EssentialService;
import com.todd.studentms.util.Tdber;

import java.util.Arrays;
import java.util.List;

public class ClassServiceImpl extends EssentialServiceImpl implements ClassService {

    @Override
    public boolean delete(java.lang.Class cls, Object[] ids) {
        try {
            Arrays.stream(ids).forEach(classId -> {
                // 班级一删, 同时删除:
                // 1. CourseArrangement 改班所有的课程安排
                Tdber.runExecute("delete from course_arrangement where class_id = ?", new Object[]{classId});
                // 2. Grade 该班学生的所有成绩信息
                List<Person> studentsOfTheClass = Tdber.query(new Person().setClassId((String) classId));
                if (studentsOfTheClass.size() > 0) {
                    studentsOfTheClass.forEach(stu -> {
                        Tdber.runExecute("delete from grade where stu_id = ?", new String[]{stu.getId()});
                    });
                    // 3. Person 该班所有学生
                    Tdber.delete(Person.class, studentsOfTheClass.stream().map(Person::getId).toArray());
                }
            });
            // 4. Class 该班
            Tdber.delete(Class.class, ids);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
