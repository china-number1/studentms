package com.todd.studentms.service.impl;

import com.todd.studentms.service.EssentialService;
import com.todd.studentms.util.Tdber;

import java.util.List;

public class EssentialServiceImpl implements EssentialService {
    @Override
    public boolean update(Object obj) {
        return Tdber.update(obj);
    }

    @Override
    public <T> List<T> get(T obj) {
        return Tdber.query(obj);
    }

    @Override
    public <T> T getOne(T obj) {
        return Tdber.queryOne(obj);
    }

    @Override
    public <T> List<T> getLike(T obj) {
        return Tdber.queryLike(obj);
    }

    @Override
    public boolean delete(Class cls, Object[] ids) {
        return Tdber.delete(cls, ids);
    }

    @Override
    public boolean delete(Class cls, Object id) {
        return Tdber.delete(cls, id);
    }

    @Override
    public boolean add(Object obj) {
        return Tdber.insert(obj);
    }


}
