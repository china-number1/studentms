package com.todd.studentms.service.impl;

import cn.hutool.core.util.StrUtil;
import com.todd.studentms.entity.Grade;
import com.todd.studentms.entity.Person;
import com.todd.studentms.entity.vo.GradeJoinPersonJoinCourse;
import com.todd.studentms.service.GradeService;
import com.todd.studentms.util.MyUtil;
import com.todd.studentms.util.Tdber;

import java.sql.Struct;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class GradeServiceImpl extends EssentialServiceImpl implements GradeService {

    @Override
    public List<GradeJoinPersonJoinCourse> getGradeByPerson(Person person) throws Exception {
        if (person == null) return null;
        if (person.getPersonTypeId().equals("persontype001") || person.getPersonTypeId().equals("persontype002")) {
            return Tdber.listMapToEntity(
                    Tdber.runQuery(
                            "select g.stu_id stu_id, g.course_id course_id ,p.`name` stu_name, c.course_name course_name,score " +
                                    "from grade g join person p on g.stu_id=p.id join course c on c.id = g.course_id;",
                            null),
                    new GradeJoinPersonJoinCourse()
            );
        } else {

            return Tdber.listMapToEntity(
                    Tdber.runQuery(
                            "select g.stu_id stu_id, g.course_id course_id ,p.`name` stu_name, c.course_name course_name,score " +
                                    "from grade g join person p on g.stu_id=p.id join course c on c.id = g.course_id " +
                                    "where stu_id = ?;",
                            new Object[]{person.getId()}),
                    new GradeJoinPersonJoinCourse()
            );
        }
    }

    // 目前只模糊查询学生的姓名和课程名, 查所有然后用java筛选
    @Override
    public List<GradeJoinPersonJoinCourse> getLike(GradeJoinPersonJoinCourse gpj, String personTypeId) throws Exception {
        return getGradeByPerson(new Person().setPersonTypeId(personTypeId))
                .stream()
                .filter(o -> {
                    String stuName = gpj.getStuName();
                    String courseName = gpj.getCourseName();
                    if (StrUtil.isNotEmpty(stuName) && StrUtil.isNotEmpty(courseName)) {
                        return o.getStuName().contains(stuName) && o.getCourseName().contains(courseName);
                    } else if (StrUtil.isNotEmpty(stuName) && StrUtil.isEmpty(courseName)) {
                        return o.getStuName().contains(stuName);
                    } else if (StrUtil.isEmpty(stuName) && StrUtil.isNotEmpty(courseName)) {
                        return o.getCourseName().contains(courseName);
                    } else {
                        return true;
                    }
                })
                .collect(Collectors.toList());
    }

    @Override
    public boolean update(Object obj) {
        GradeJoinPersonJoinCourse gpj = (GradeJoinPersonJoinCourse) obj;
        return Tdber.runExecute(
                "update grade set score = ? where stu_id = ? and course_id = ?;",
                new Object[]{gpj.getScore(), gpj.getStuId(), gpj.getCourseId()}
        );
    }
}
