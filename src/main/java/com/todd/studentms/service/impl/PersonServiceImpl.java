package com.todd.studentms.service.impl;

import com.todd.studentms.entity.Grade;
import com.todd.studentms.entity.Person;
import com.todd.studentms.entity.PersonType;
import com.todd.studentms.service.PersonService;
import com.todd.studentms.util.MyUtil;
import com.todd.studentms.util.Tdber;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class PersonServiceImpl extends EssentialServiceImpl implements PersonService{
    @Override
    public boolean delete(Class cls, Object[] stuIds) {
        List<Boolean> isOks = new ArrayList();
        try {
            // 删学生前, 先删除对应的成绩信息
            Arrays.stream(stuIds).forEach(stuId -> {
                // 删成绩
                Tdber.runExecute("delete from grade where stu_id = ? ",new Object[]{stuId});
                // 删学生
                isOks.add(super.delete(Person.class, stuId));
            });
            // 上述操作是否全部成功
            return isOks.stream().allMatch(Boolean::booleanValue);
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }
}
