package com.todd.studentms.service.impl;

import com.todd.studentms.entity.Ability;
import com.todd.studentms.service.AbilityService;
import com.todd.studentms.util.Tdber;

import java.util.List;
import java.util.stream.Collectors;

public class AbilityServiceImpl extends EssentialServiceImpl implements AbilityService {
    @Override
    public List<String> getBilities(String personTypeId) throws Exception {
        return Tdber.listMapToEntity(Tdber.runQuery(
                "select a.id id, a.ability_code ability_code, a.remark remark " +
                        "from ability_distribution ad join ability a on ad.ability_id = a.id " +
                        "where person_type_id = ?", new Object[]{personTypeId}), new Ability())
                .stream()
                .map(a -> a.getAbilityCode())
                .collect(Collectors.toList());
    }
}
