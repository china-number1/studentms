package com.todd.studentms.service.impl;

import cn.hutool.core.util.IdUtil;
import com.todd.studentms.entity.CourseArrangement;
import com.todd.studentms.entity.Grade;
import com.todd.studentms.entity.Person;
import com.todd.studentms.service.CourseService;
import com.todd.studentms.util.Tdber;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

public class CourseServiceImpl extends EssentialServiceImpl implements CourseService {

    @Override
    public List<CourseArrangement> getArrangement(CourseArrangement ca, Person curPerson) {
        String personTypeId = curPerson.getPersonTypeId();
        // 管理员和教师得到所有信息
        if (personTypeId.equals("persontype001") || personTypeId.equals("persontype002")) {
            return super.get(ca);
        } else {
            // 学生得到自己班要上的课程信息
            return super.get(ca.setClassId(curPerson.getClassId()));
        }
    }

    @Override
    public boolean addArrangement(CourseArrangement ca) {
        return super.add(ca);
    }

    @Override
    public boolean elect(String curPersonId, String cid) {
        return super.add(new Grade().setStuId(curPersonId).setCourseId(cid).setScore(new BigDecimal(0.00f)));
    }
}
