package com.todd.studentms.entity.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class GradeJoinPersonJoinCourse {
    private String stuId;
    private String courseId;
    private String stuName;
    private String courseName;
    private BigDecimal score;
}
