package com.todd.studentms.entity;

import com.todd.studentms.util.ID;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Grade {
    private String stuId;
    private String courseId;
    private BigDecimal score;
}
