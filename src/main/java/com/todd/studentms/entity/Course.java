package com.todd.studentms.entity;

import com.todd.studentms.util.ID;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@Accessors(chain = true)
public class Course {
    @ID
    private String id;
    private String courseName;
    private Integer totalHours;
    private Integer credit;
}
