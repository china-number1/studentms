package com.todd.studentms.entity;

import com.todd.studentms.util.ID;
import com.todd.studentms.util.NonParticipation;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Value;
import lombok.experimental.Accessors;

import java.lang.annotation.Documented;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Person {
    @ID
    private String id;
    private String jobNo;
    private String password;
    private String name;
    private String gender;
    private Date birthDay;
    private String address;
    private String phone;
    private Integer enrollmentYear;
    private String classId;
    private String personTypeId;
    private String remark;

    @NonParticipation
    private Class classInfo;
    @NonParticipation
    private PersonType personType;
}
