package com.todd.studentms.entity;

import com.todd.studentms.util.ID;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDate;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Class {
    @ID
    private String id;
    private String className;
    private Integer count;
    private Date createDate;
    private String majorId;
    private String instructorId;
}
