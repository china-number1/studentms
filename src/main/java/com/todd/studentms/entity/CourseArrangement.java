package com.todd.studentms.entity;

import cn.hutool.core.date.DateTime;
import com.todd.studentms.util.ID;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class CourseArrangement {
    @ID
    private String id;
    private String classId;
    private String courseId;
    private String teacherId;
    private Date attendTime;
    private String attendPlace;
}
