package com.todd.studentms.entity;

import com.todd.studentms.util.ID;
import com.todd.studentms.util.NonParticipation;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
public class Ability {
    @ID
    private String id;
    private String abilityCode;
    private String remark;
}
