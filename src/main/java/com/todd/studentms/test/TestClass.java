package com.todd.studentms.test;

import com.todd.studentms.entity.Person;
import com.todd.studentms.util.Tdber;

public class TestClass {
    public static void main(String[] args) {
        Tdber.query(new Person().setAddress("台湾省台北市").setGender("男"))
                .forEach(System.out::println);
    }
}
