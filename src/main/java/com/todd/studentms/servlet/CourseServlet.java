package com.todd.studentms.servlet;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.todd.studentms.entity.Course;
import com.todd.studentms.entity.CourseArrangement;
import com.todd.studentms.entity.Person;
import com.todd.studentms.service.impl.CourseServiceImpl;
import com.todd.studentms.service.impl.PersonServiceImpl;
import com.todd.studentms.util.MyUtil;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.sql.Struct;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

@WebServlet("/course/*")
public class CourseServlet extends EssentialServlet {

    // /get
    public List<Course> get(JSONObject jsonObj, HttpServletRequest req, HttpServletResponse res) {
        return new CourseServiceImpl().get(JSONUtil.toBean(jsonObj, Course.class));
    }

    // /getlike
    public List<Course> getlike(JSONObject jsonObj, HttpServletRequest req, HttpServletResponse res) {
        return new CourseServiceImpl().getLike(JSONUtil.toBean(jsonObj, Course.class));
    }

    // /add
    public boolean add(JSONObject jsonObje, HttpServletRequest req, HttpServletResponse res) {
        return new CourseServiceImpl().add(JSONUtil.toBean(jsonObje, Course.class).setId(MyUtil.randUUID()));
    }

    // /upd
    public boolean upd(JSONObject jsonObj, HttpServletRequest req, HttpServletResponse res) {
        return new CourseServiceImpl().update(JSONUtil.toBean(jsonObj, Course.class));
    }

    // /del
    public boolean doDelete(String[] ids, HttpServletRequest req, HttpServletResponse res) {
        return new CourseServiceImpl().delete(Course.class, ids);
    }

    // 获得课程安排
    // getarrangement  传perosn的id过来就可以
    public List<CourseArrangement> getarrangement(JSONObject jsonObj, HttpServletRequest req, HttpServletResponse res) {
        Person curPerson = new PersonServiceImpl().getOne(new Person().setId(req.getHeader("Cur-Person-Id")));
        List<CourseArrangement> courseArrangements = new CourseServiceImpl().getArrangement(JSONUtil.toBean(jsonObj, CourseArrangement.class), curPerson);
        courseArrangements.sort((o1,o2)->o1.getAttendTime().compareTo(o2.getAttendTime()));
        return courseArrangements;
    }

    // 添加课程安排
    // /addarrangement
    public boolean addarrangement(JSONObject jsonObj, HttpServletRequest req, HttpServletResponse res) {
        return new CourseServiceImpl().addArrangement(JSONUtil.toBean(jsonObj, CourseArrangement.class).setId(IdUtil.fastSimpleUUID()));
    }

    // 修改课程信息
    // /updatearrangement
    public boolean updatearrangement(JSONObject jsonObje, HttpServletRequest req, HttpServletResponse res){
        return new CourseServiceImpl().update(JSONUtil.toBean(jsonObje,CourseArrangement.class));
    }

    // 删除课程安排 (POST请求, 参数: {ids:[...]} )
    // /delarrangement
    public boolean delarrangement(JSONObject jsonObj, HttpServletRequest req, HttpServletResponse res) {
        return new CourseServiceImpl().delete(CourseArrangement.class, jsonObj.getJSONArray("ids").toList(String.class).toArray());
    }

    // 选修课程
    // /elect
    public boolean elect(JSONObject jsonObj,HttpServletRequest req, HttpServletResponse res){
        String curPersonId = req.getHeader("Cur-Person-Id");
        String cid = jsonObj.getStr("cid");
        if(StrUtil.isEmpty(curPersonId)){
            throw new RuntimeException("header没有传Cur-Person-Id");
        }else if(StrUtil.isEmpty(cid)){
            throw  new RuntimeException("get没有传cid");
        }
        return new CourseServiceImpl().elect(curPersonId,cid);
    }
}
