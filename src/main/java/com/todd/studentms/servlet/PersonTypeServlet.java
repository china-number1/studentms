package com.todd.studentms.servlet;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.todd.studentms.entity.Class;
import com.todd.studentms.entity.PersonType;
import com.todd.studentms.service.impl.ClassServiceImpl;
import com.todd.studentms.service.impl.PersonTypeServiceImpl;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@WebServlet("/persontype/*")
public class PersonTypeServlet extends EssentialServlet {
    public List<PersonType> get(JSONObject jsonObj, HttpServletRequest req, HttpServletResponse res) {
        PersonType personType = JSONUtil.toBean(jsonObj, PersonType.class);
        System.out.println("AcceptEntity  >>> " + personType);
        return new PersonTypeServiceImpl().get(personType);

    }
}
