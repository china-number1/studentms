package com.todd.studentms.servlet;

import cn.hutool.json.JSONObject;
import com.todd.studentms.service.impl.AbilityServiceImpl;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

@WebServlet("/ability/*")
public class AbilityServlet extends EssentialServlet {
    //  /getbilities
    public List<String> getbilities(JSONObject jsonObj, HttpServletRequest req, HttpServletResponse res) throws Exception {
        return new AbilityServiceImpl().getBilities(jsonObj.getStr("personTypeId"));
    }
}
