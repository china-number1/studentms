package com.todd.studentms.servlet;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.todd.studentms.entity.Major;
import com.todd.studentms.entity.Major;
import com.todd.studentms.service.impl.MajorServiceImpl;
import com.todd.studentms.service.impl.MajorServiceImpl;
import com.todd.studentms.util.MyUtil;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

@WebServlet("/major/*")
public class MajorServlet extends EssentialServlet {
    //  /get
    public List<Major> get(JSONObject jsonObj, HttpServletRequest req, HttpServletResponse res){
        Major major = JSONUtil.toBean(jsonObj,Major.class);
        System.out.println("AcceptEntity  >>> "+major);
        return new MajorServiceImpl().get(major);
    }

    // /getlike
    public List<Major> getlike(JSONObject jsonObj, HttpServletRequest req, HttpServletResponse res){
        Major major = JSONUtil.toBean(jsonObj,Major.class);
        System.out.println("AcceptEntity  >>> "+major);
        return new MajorServiceImpl().getLike(major);
    }

    // /add
    public boolean add(JSONObject jsonObj,HttpServletRequest req,HttpServletResponse res){
        Major major = JSONUtil.toBean(jsonObj,Major.class);
        System.out.println("AcceptEntity  >>> "+major);
        return new MajorServiceImpl().add(major.setId(MyUtil.randUUID()));
    }

    // /del
    public boolean doDelete(String[] ids,HttpServletRequest req,HttpServletResponse res){
        System.out.print("AcceptArray  >>> ");
        Arrays.stream(ids).forEach(id->System.out.print(id+" "));
        System.out.println();
        return new MajorServiceImpl().delete(Major.class,ids);
    }

    // /upd
    public boolean upd(JSONObject jsonObj,HttpServletRequest req,HttpServletResponse res){
        Major major = JSONUtil.toBean(jsonObj,Major.class);
        System.out.println("AcceptEntity  >>> "+major);
        return new MajorServiceImpl().update(major);
    }
}
