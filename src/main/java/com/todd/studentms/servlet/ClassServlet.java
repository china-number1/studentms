package com.todd.studentms.servlet;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.todd.studentms.entity.Class;
import com.todd.studentms.service.impl.ClassServiceImpl;
import com.todd.studentms.util.MyUtil;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

@WebServlet("/class/*")
public class ClassServlet extends EssentialServlet {
    public List<Class> get(JSONObject jsonObj, HttpServletRequest req, HttpServletResponse res) {
        Class classInfo = JSONUtil.toBean(jsonObj, Class.class);
        System.out.println("AcceptEntity  >>> " + classInfo);
        return new ClassServiceImpl().get(classInfo);

    }

    // /getlike
    public List<Class> getlike(JSONObject jsonObj, HttpServletRequest req, HttpServletResponse res){
        Class classInfo = JSONUtil.toBean(jsonObj,Class.class);
        System.out.println("AcceptEntity  >>> "+classInfo);
        return new ClassServiceImpl().getLike(classInfo);
    }

    // /add
    public boolean add(JSONObject jsonObj,HttpServletRequest req,HttpServletResponse res){
        Class classInfo = JSONUtil.toBean(jsonObj,Class.class);
        System.out.println("AcceptEntity  >>> "+classInfo);
        return new ClassServiceImpl().add(classInfo.setId(MyUtil.randUUID()));
    }

    // /del
    public boolean doDelete(String[] ids,HttpServletRequest req,HttpServletResponse res){
        System.out.print("AcceptArray  >>> ");
        Arrays.stream(ids).forEach(id->System.out.print(id+" "));
        System.out.println();
        return new ClassServiceImpl().delete(Class.class,ids);
    }

    // /upd
    public boolean upd(JSONObject jsonObj,HttpServletRequest req,HttpServletResponse res){
        Class classInfo = JSONUtil.toBean(jsonObj,Class.class);
        System.out.println("AcceptEntity  >>> "+classInfo);
        return new ClassServiceImpl().update(classInfo);
    }
}
