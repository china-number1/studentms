package com.todd.studentms.util;

import javax.servlet.http.HttpSession;
import java.util.HashMap;

public class SessionUtil {
    private static SessionUtil instance;
    private HashMap<String,HttpSession> sessionMap;

    private SessionUtil() {
        sessionMap = new HashMap<String, HttpSession>();
    }

    public static SessionUtil getInstance() {
        if (instance == null) {
            instance = new SessionUtil();
        }
        return instance;
    }

    public synchronized void addSession(HttpSession session) {
        if (session != null) {
            sessionMap.put(session.getId(), session);
        }
    }

    public synchronized void delSession(HttpSession session) {
        if (session != null) {
            sessionMap.remove(session.getId());
        }
    }

    public synchronized HttpSession getSession(String sessionID) {
        if (sessionID == null) {
            return null;
        }
        return sessionMap.get(sessionID);
    }

}  
