package com.todd.studentms.util;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import java.lang.String;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Map;
import java.util.UUID;

public class MyUtil {
    /**
     * 将request参数值转为json
     */

//    public static JSONObject request2Json(HttpServletRequest request) {
//        JSONObject requestJson = new JSONObject();
//        Enumeration paramNames = request.getParameterNames();
//        while (paramNames.hasMoreElements()) {
//            String paramName = (String) paramNames.nextElement();
//            String[] pv = request.getParameterValues(paramName);
//            StringBuilder sb = new StringBuilder();
//            for (int i = 0; i < pv.length; i++) {
//                if (pv[i].length() > 0) {
//                    if (i > 0) {
//                        sb.append(",");
//                    }
//                    sb.append(pv[i]);
//                }
//            }
//            requestJson.put(paramName, sb.toString());
//        }
//        return requestJson;
//    }

    // GET请求参数转JSON
    public static JSONObject GETRequest2Json(HttpServletRequest req) throws IOException {
        JSONObject jsonObj = JSONUtil.createObj();
        Map<String, String[]> map = req.getParameterMap();
        map.keySet().forEach(key -> {
            String[] values = map.get(key);
            if (values.length == 1 && !"".equals(values[0])) {
                jsonObj.put(key, values[0]);
            } else if (Arrays.stream(values).allMatch(s -> !"".equals(s))) {
                jsonObj.put(key, values);
            }
        });
        return jsonObj;
    }

    // POST请求体转JSON
    public static JSONObject POSTRequest2Json(HttpServletRequest req) throws IOException{
        JSONObject jsonObj = JSONUtil.createObj();
        StringBuffer sb = new StringBuffer("");
        BufferedReader br = new BufferedReader(new InputStreamReader((ServletInputStream) req.getInputStream(), "UTF-8"));
        String temp;
        while ((temp=br.readLine())!=null){
            sb.append(temp);
        }
        return JSONUtil.parseObj(new String(sb));
    }

    // 该函数是为了给delete请求用, 返回一个id的列表
    public static String[] request2StringArr(HttpServletRequest req) throws IOException {
        StringBuffer sb = new StringBuffer("");
        BufferedReader br = new BufferedReader(new InputStreamReader((ServletInputStream) req.getInputStream(), "UTF-8"));
        String temp;
        while ((temp = br.readLine()) != null){
            sb.append(temp);
        }
        String idArr = new String(sb);
        return idArr.replaceAll("\\[","")
                .replaceAll("\\]","")
                .replaceAll("\"","")
                .split(",");
    }

    public static String randUUID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }
}
