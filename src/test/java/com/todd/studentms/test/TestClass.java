package com.todd.studentms.test;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.sun.xml.internal.bind.v2.model.core.ClassInfo;
import com.todd.studentms.entity.Class;
import com.todd.studentms.entity.CourseArrangement;
import com.todd.studentms.entity.Person;
import com.todd.studentms.service.EssentialService;
import com.todd.studentms.service.impl.EssentialServiceImpl;
import com.todd.studentms.service.impl.PersonServiceImpl;
import com.todd.studentms.util.MyUtil;
import com.todd.studentms.util.Tdber;
import lombok.experimental.Accessors;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

public class TestClass {

    @Test
    public void testNewQuery() {
        System.out.println(new PersonServiceImpl().get(new Person().setAddress("湖北省孝感市")));
    }

    @Test
    public void testNewQueryOne() {
        System.out.println(new PersonServiceImpl().get(new Person()));
    }

    @Test
    public void testNewUpdate() {
        System.out.println(new PersonServiceImpl().update(new Person().setId("stu00test").setName("Hello")));
    }

    @Test
    public void testNewInsert() {
        System.out.println(new PersonServiceImpl().add(
                new Person()
                        .setId("stu00test2")
                        .setJobNo("020123")
                        .setPassword("aaa111")
                        .setName("新学生")
                        .setGender("女")
                        .setBirthDay(new Date())
                        .setAddress("湖北省孝感市")
                        .setPhone("11111")
                        .setEnrollmentYear(2020)
                        .setRemark("马保国粉丝")
        ));
    }

    @Test
    public void testNewDelete(){
        System.out.println(new PersonServiceImpl().delete(Person.class,new Object[]{"stu00test3","stu00test4","stu00test5"}));
    }

    @Test
    public void testJSONObject(){
        JSONObject jsonObj = JSONUtil.createObj();
        jsonObj.put("name",null);
        jsonObj.put("age",18);
        jsonObj.put("gender","男");
        System.out.println(JSONUtil.toBean(jsonObj,Person.class));
    }

    @Test
    public void testHutoolUUID(){
        System.out.println(IdUtil.fastSimpleUUID());
    }

    // 更新每个班级的人数情况
    @Test
    public void refreshCountOfMembersOfClasses(){
        EssentialService service = new EssentialServiceImpl();
        service.get(new Class()).forEach(classInfo->{
            service.update(new Class().setId(classInfo.getId()).setCount(service.get(new Person().setClassId(classInfo.getId()).setPersonTypeId("persontype003")).size()));
        });
    }
}
