/*
 Navicat Premium Data Transfer

 Source Server         : Local MySQL
 Source Server Type    : MySQL
 Source Server Version : 50721
 Source Host           : localhost:3306
 Source Schema         : studentms

 Target Server Type    : MySQL
 Target Server Version : 50721
 File Encoding         : 65001

 Date: 27/06/2021 17:33:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ability
-- ----------------------------
DROP TABLE IF EXISTS `ability`;
CREATE TABLE `ability`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ability_code` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `remark` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ability
-- ----------------------------
INSERT INTO `ability` VALUES ('ability001', 'SELECT_STUDENT', '查询学生信息权限');
INSERT INTO `ability` VALUES ('ability002', 'SELECT_CLASS', '查询班级信息权限');
INSERT INTO `ability` VALUES ('ability003', 'SELECT_GRADE', '查询成绩信息权限');
INSERT INTO `ability` VALUES ('ability004', 'UPDATE_STUDENT', '修改学生信息权限');
INSERT INTO `ability` VALUES ('ability005', 'UPDATE_CLASS', '修改班级信息权限');
INSERT INTO `ability` VALUES ('ability006', 'UPDATE_GRADE', '修改成绩信息权限');
INSERT INTO `ability` VALUES ('ability007', 'DELETE_STUDENT', '删除学生信息权限');
INSERT INTO `ability` VALUES ('ability008', 'DELETE_CLASS', '删除班级信息权限');
INSERT INTO `ability` VALUES ('ability009', 'DELETE_GRADE', '删除成绩信息权限');
INSERT INTO `ability` VALUES ('ability010', 'ADD_STUDENT', '增加学生信息权限');
INSERT INTO `ability` VALUES ('ability011', 'ADD_CLASS', '增加班级信息权限');
INSERT INTO `ability` VALUES ('ability012', 'ADD_GRADE', '增加成绩信息权限');
INSERT INTO `ability` VALUES ('ability013', 'ADD_COURSE', '增加课程信息权限');
INSERT INTO `ability` VALUES ('ability014', 'UPDATE_COURSE', '修改课程信息权限');
INSERT INTO `ability` VALUES ('ability015', 'DELETE_COURSE', '删除课程信息权限');
INSERT INTO `ability` VALUES ('ability016', 'ADD_COURSE_ARRANGEMENT', '增加课程安排信息权限');
INSERT INTO `ability` VALUES ('ability017', 'DELETE_COURSE_ARRANGEMENT', '删除课程安排信息权限');
INSERT INTO `ability` VALUES ('ability018', 'UPDATE_COURSE_ARRANGEMENT', '修改课程安排信息权限');

-- ----------------------------
-- Table structure for ability_distribution
-- ----------------------------
DROP TABLE IF EXISTS `ability_distribution`;
CREATE TABLE `ability_distribution`  (
  `person_type_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `ability_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`person_type_id`, `ability_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ability_distribution
-- ----------------------------
INSERT INTO `ability_distribution` VALUES ('persontype001', 'ability001');
INSERT INTO `ability_distribution` VALUES ('persontype001', 'ability002');
INSERT INTO `ability_distribution` VALUES ('persontype001', 'ability003');
INSERT INTO `ability_distribution` VALUES ('persontype001', 'ability004');
INSERT INTO `ability_distribution` VALUES ('persontype001', 'ability005');
INSERT INTO `ability_distribution` VALUES ('persontype001', 'ability006');
INSERT INTO `ability_distribution` VALUES ('persontype001', 'ability007');
INSERT INTO `ability_distribution` VALUES ('persontype001', 'ability008');
INSERT INTO `ability_distribution` VALUES ('persontype001', 'ability009');
INSERT INTO `ability_distribution` VALUES ('persontype001', 'ability010');
INSERT INTO `ability_distribution` VALUES ('persontype001', 'ability011');
INSERT INTO `ability_distribution` VALUES ('persontype001', 'ability012');
INSERT INTO `ability_distribution` VALUES ('persontype001', 'ability013');
INSERT INTO `ability_distribution` VALUES ('persontype001', 'ability014');
INSERT INTO `ability_distribution` VALUES ('persontype001', 'ability015');
INSERT INTO `ability_distribution` VALUES ('persontype001', 'ability016');
INSERT INTO `ability_distribution` VALUES ('persontype001', 'ability017');
INSERT INTO `ability_distribution` VALUES ('persontype001', 'ability018');
INSERT INTO `ability_distribution` VALUES ('persontype002', 'ability001');
INSERT INTO `ability_distribution` VALUES ('persontype002', 'ability002');
INSERT INTO `ability_distribution` VALUES ('persontype002', 'ability003');
INSERT INTO `ability_distribution` VALUES ('persontype002', 'ability004');
INSERT INTO `ability_distribution` VALUES ('persontype002', 'ability005');
INSERT INTO `ability_distribution` VALUES ('persontype002', 'ability006');
INSERT INTO `ability_distribution` VALUES ('persontype002', 'ability007');
INSERT INTO `ability_distribution` VALUES ('persontype002', 'ability008');
INSERT INTO `ability_distribution` VALUES ('persontype002', 'ability009');
INSERT INTO `ability_distribution` VALUES ('persontype002', 'ability010');
INSERT INTO `ability_distribution` VALUES ('persontype002', 'ability011');
INSERT INTO `ability_distribution` VALUES ('persontype002', 'ability012');
INSERT INTO `ability_distribution` VALUES ('persontype002', 'ability013');
INSERT INTO `ability_distribution` VALUES ('persontype002', 'ability014');
INSERT INTO `ability_distribution` VALUES ('persontype002', 'ability015');
INSERT INTO `ability_distribution` VALUES ('persontype002', 'ability016');
INSERT INTO `ability_distribution` VALUES ('persontype002', 'ability017');
INSERT INTO `ability_distribution` VALUES ('persontype002', 'ability018');
INSERT INTO `ability_distribution` VALUES ('persontype003', 'ability001');
INSERT INTO `ability_distribution` VALUES ('persontype003', 'ability002');
INSERT INTO `ability_distribution` VALUES ('persontype003', 'ability003');

-- ----------------------------
-- Table structure for class
-- ----------------------------
DROP TABLE IF EXISTS `class`;
CREATE TABLE `class`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `class_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `count` int(11) NULL DEFAULT NULL,
  `create_date` date NULL DEFAULT NULL,
  `major_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `instructor_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of class
-- ----------------------------
INSERT INTO `class` VALUES ('12040e2ab1704d96b7029a40e6f0fee1', '马克思学1班', 10, '2020-12-01', 'major005', 'teacher001');
INSERT INTO `class` VALUES ('1370dcf623dd4a39a05150ae9dc88d83', '会计2班', 11, '2020-12-01', 'major004', '4e2cd399c5dd496fb7715872ca6ec496');
INSERT INTO `class` VALUES ('345f0b0ef5994f06b02d9627b873516e', '英语3班', 8, '2020-12-01', 'major007', 'teacher002');
INSERT INTO `class` VALUES ('45b974a01ba14033b6197d28b2848f3e', '美术2班', 13, '2020-12-01', 'major008', 'c65665ddda1f4f08ae9cbf61a2db5968');
INSERT INTO `class` VALUES ('596e702819b54f89a2b3dfe9b7036386', '英语4班', 6, '2020-12-01', 'major007', 'teacher002');
INSERT INTO `class` VALUES ('72ba39338b9c4acd8b6a9f44963bd3ca', '核物理1班', 18, '2020-11-11', 'major003', 'teacher002');
INSERT INTO `class` VALUES ('78d719c0a90b4199bae67a13423a69ae', '会计4班', 16, '2020-12-01', 'major004', 'c65665ddda1f4f08ae9cbf61a2db5968');
INSERT INTO `class` VALUES ('9cdd656fe33245dbaff646f7d9c107df', '哲学2班', 11, '2020-12-01', 'major011', '4e2cd399c5dd496fb7715872ca6ec496');
INSERT INTO `class` VALUES ('9f9bb21e92674ce5a1dafbdc90a2e926', '数学1班', 13, '2020-12-01', 'major010', 'teacher001');
INSERT INTO `class` VALUES ('a456d7f3f0ae45279c0357837a9fca3f', '英语5班', 10, '2020-12-01', 'major007', '4e2cd399c5dd496fb7715872ca6ec496');
INSERT INTO `class` VALUES ('a5a24dfe5f1e433f9419d31dd805f2d3', '体育1班', 9, '2020-12-01', 'major006', 'teacher002');
INSERT INTO `class` VALUES ('bd21b35a6cd141e4ac18b4c26df8f01b', '教育心理学3班', 8, '2020-11-04', 'major002', 'teacher002');
INSERT INTO `class` VALUES ('class001', '软件工程4班', 27, '2020-09-29', 'major001', 'teacher001');
INSERT INTO `class` VALUES ('class002', '教育心理学1班', 8, '2020-09-29', 'major002', 'teacher001');
INSERT INTO `class` VALUES ('d44c5c50b5304b819a9454dfdb130454', '美术学1班', 14, '2020-12-01', 'major008', '4e2cd399c5dd496fb7715872ca6ec496');
INSERT INTO `class` VALUES ('ea19aa4cabf64c95a4d5d17d8ee50550', '核物理1班', 10, '2020-11-17', 'major003', 'teacher001');
INSERT INTO `class` VALUES ('eca731bc0bc24c3fa158736506dd5f8a', '金融学1班', 8, '2020-12-01', 'major009', '4e2cd399c5dd496fb7715872ca6ec496');
INSERT INTO `class` VALUES ('f90661529755477389909ecc0bc9bf13', '哲学1班', 21, '2020-11-12', 'major011', 'teacher002');
INSERT INTO `class` VALUES ('fb7cb4caf4474de985462172b00e96c0', '金融学2班', 3, '2020-12-01', 'major009', 'c65665ddda1f4f08ae9cbf61a2db5968');

-- ----------------------------
-- Table structure for course
-- ----------------------------
DROP TABLE IF EXISTS `course`;
CREATE TABLE `course`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `course_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `total_hours` int(11) NULL DEFAULT NULL,
  `credit` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of course
-- ----------------------------
INSERT INTO `course` VALUES ('04ad69c58ee94dd081c67c70b2941156', '大学体育', 10, 10);
INSERT INTO `course` VALUES ('0ae613c306544c958fa95583d8b960a3', '大学英语', 30, 50);
INSERT INTO `course` VALUES ('148b44d0fd014cda97dce5b591eb5178', '毛泽东思想概论', 20, 30);
INSERT INTO `course` VALUES ('2bf3d3415d0c4246883d7ac777097cb8', '服装设计原理', 10, 5);
INSERT INTO `course` VALUES ('3c7d6e08f60b4dc1b9bad01abd2198fa', '机器学习', 50, 50);
INSERT INTO `course` VALUES ('40415ddf793f480c9ce49eb47537d108', '护士实践', 100, 5);
INSERT INTO `course` VALUES ('5fcc39fa2da04dfc9b01895e7676bb9f', '母猪产后护理实践', 30, 50);
INSERT INTO `course` VALUES ('6012d8dd64f1480081ba5d5b6da35b09', 'VC++程序设计', 10, 5);
INSERT INTO `course` VALUES ('7c00e4630f974ce79b183e7a07602f25', '计算机网络', 30, 20);
INSERT INTO `course` VALUES ('80612d2aad4a4a5da8884a06ef7ffc42', '量子力学', 30, 100);
INSERT INTO `course` VALUES ('8dcab2301bab40dd9c0c6e2224aa00d0', '心理健康教育', 10, 10);
INSERT INTO `course` VALUES ('9dab5f92764142cfb7bd9fccdf379a85', 'JSP开发', 20, 5);
INSERT INTO `course` VALUES ('a5dbe9c2392f4f4c8a5bc65f34dd1a1c', '爱因斯坦物理导论', 100, 100);
INSERT INTO `course` VALUES ('a920903b2d71490a9912b14768513bd2', '应用文与写作', 20, 20);
INSERT INTO `course` VALUES ('ca71e4808fa34d6ea000c821d1c81d32', '美术绘图', 10, 10);
INSERT INTO `course` VALUES ('ce31ab86829f4906af452c013aae2eb8', '小学教育体系', 10, 10);
INSERT INTO `course` VALUES ('ce9f4e9a477b459ca25c08b713d0751c', '马克思理论', 30, 100);
INSERT INTO `course` VALUES ('course001', '数据库原理', 40, 50);
INSERT INTO `course` VALUES ('course002', '心理学', 55, 110);

-- ----------------------------
-- Table structure for course_arrangement
-- ----------------------------
DROP TABLE IF EXISTS `course_arrangement`;
CREATE TABLE `course_arrangement`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `class_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `course_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `teacher_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `attend_time` datetime(0) NULL DEFAULT NULL,
  `attend_place` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of course_arrangement
-- ----------------------------
INSERT INTO `course_arrangement` VALUES ('258b3625887441069787c80c7e20add5', 'ea19aa4cabf64c95a4d5d17d8ee50550', '0ae613c306544c958fa95583d8b960a3', 'teacher002', '2020-12-22 16:30:00', '外语楼201');
INSERT INTO `course_arrangement` VALUES ('2a3abfd7222d422b807514904a3804d5', '72ba39338b9c4acd8b6a9f44963bd3ca', '3c7d6e08f60b4dc1b9bad01abd2198fa', '4e2cd399c5dd496fb7715872ca6ec496', '2020-11-27 16:37:00', '教三楼402');
INSERT INTO `course_arrangement` VALUES ('54bfe5cb7df740a48c2a8e2e8086061a', 'f90661529755477389909ecc0bc9bf13', '148b44d0fd014cda97dce5b591eb5178', '4e2cd399c5dd496fb7715872ca6ec496', '2020-11-30 16:30:00', '教一楼408');
INSERT INTO `course_arrangement` VALUES ('61471066841c467f85d41e3e04a3db4b', '72ba39338b9c4acd8b6a9f44963bd3ca', '3c7d6e08f60b4dc1b9bad01abd2198fa', '4e2cd399c5dd496fb7715872ca6ec496', '2020-11-24 16:36:00', '教三楼402');
INSERT INTO `course_arrangement` VALUES ('614a3824562246b4af7bc13884078b79', 'class001', '3c7d6e08f60b4dc1b9bad01abd2198fa', 'teacher001', '2020-11-22 22:37:19', '机器人实验室2');
INSERT INTO `course_arrangement` VALUES ('783c51dc09a445f9a5ed24b53c00dd30', 'f90661529755477389909ecc0bc9bf13', '04ad69c58ee94dd081c67c70b2941156', 'c65665ddda1f4f08ae9cbf61a2db5968', '2020-11-30 17:31:00', '本院操场');
INSERT INTO `course_arrangement` VALUES ('arrange001', 'class001', 'course001', 'teacher001', '2020-11-16 08:30:00', '教一楼303');
INSERT INTO `course_arrangement` VALUES ('arrange002', 'class002', 'course002', 'teacher002', '2020-11-16 08:30:23', '教二楼501');
INSERT INTO `course_arrangement` VALUES ('b97120e5318145e48bacd097813a1e38', 'f90661529755477389909ecc0bc9bf13', 'a920903b2d71490a9912b14768513bd2', '4e2cd399c5dd496fb7715872ca6ec496', '2020-11-22 22:38:14', '经法楼103');
INSERT INTO `course_arrangement` VALUES ('c207a127ecb749eeb5ca231a5d8962f8', 'bd21b35a6cd141e4ac18b4c26df8f01b', 'ca71e4808fa34d6ea000c821d1c81d32', 'teacher001', '2020-11-30 16:31:00', '教五楼404');
INSERT INTO `course_arrangement` VALUES ('cda867b186e74e72a8465db62dda8aa0', 'class001', '0ae613c306544c958fa95583d8b960a3', 'teacher001', '2020-11-23 14:27:52', '教一楼505');
INSERT INTO `course_arrangement` VALUES ('cf38bd70e0644bffa620cdbcd84dda7d', 'f90661529755477389909ecc0bc9bf13', '3c7d6e08f60b4dc1b9bad01abd2198fa', 'c65665ddda1f4f08ae9cbf61a2db5968', '2020-11-30 17:31:00', '机器人实验室201');
INSERT INTO `course_arrangement` VALUES ('e63a6cc14f704c218f0e4d3b3ff5d3bd', 'ea19aa4cabf64c95a4d5d17d8ee50550', 'ce9f4e9a477b459ca25c08b713d0751c', 'teacher002', '2020-11-19 00:02:02', '乐学楼505');

-- ----------------------------
-- Table structure for grade
-- ----------------------------
DROP TABLE IF EXISTS `grade`;
CREATE TABLE `grade`  (
  `stu_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `course_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `score` decimal(5, 2) NULL DEFAULT NULL,
  PRIMARY KEY (`stu_id`, `course_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of grade
-- ----------------------------
INSERT INTO `grade` VALUES ('035e3880e0914e8c8bef950846bf9d68', '04ad69c58ee94dd081c67c70b2941156', 15.00);
INSERT INTO `grade` VALUES ('035e3880e0914e8c8bef950846bf9d68', '40415ddf793f480c9ce49eb47537d108', 66.00);
INSERT INTO `grade` VALUES ('0eb8bba1c29140cbbc4fffb55da08d4b', 'course001', 39.00);
INSERT INTO `grade` VALUES ('181c1fdd47394f439609bedd227d6544', 'course001', 80.00);
INSERT INTO `grade` VALUES ('181c1fdd47394f439609bedd227d6544', 'course002', 58.50);
INSERT INTO `grade` VALUES ('1e121d66f8754e5390d550455cd4612c', '0ae613c306544c958fa95583d8b960a3', 100.00);
INSERT INTO `grade` VALUES ('1e121d66f8754e5390d550455cd4612c', '148b44d0fd014cda97dce5b591eb5178', 69.00);
INSERT INTO `grade` VALUES ('1e121d66f8754e5390d550455cd4612c', '3c7d6e08f60b4dc1b9bad01abd2198fa', 100.00);
INSERT INTO `grade` VALUES ('1e121d66f8754e5390d550455cd4612c', '7c00e4630f974ce79b183e7a07602f25', 0.00);
INSERT INTO `grade` VALUES ('1e121d66f8754e5390d550455cd4612c', '80612d2aad4a4a5da8884a06ef7ffc42', 0.00);
INSERT INTO `grade` VALUES ('4a687af5eb3244f3b4d0e13929fe6dad', '6012d8dd64f1480081ba5d5b6da35b09', 99.00);
INSERT INTO `grade` VALUES ('73184e2ccf684fed854a79e9c08c713c', '0ae613c306544c958fa95583d8b960a3', 50.00);
INSERT INTO `grade` VALUES ('stu001', 'course001', 0.00);
INSERT INTO `grade` VALUES ('stu002', 'course002', 98.56);

-- ----------------------------
-- Table structure for major
-- ----------------------------
DROP TABLE IF EXISTS `major`;
CREATE TABLE `major`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `major_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of major
-- ----------------------------
INSERT INTO `major` VALUES ('major001', '软件工程');
INSERT INTO `major` VALUES ('major002', '教育心理学');
INSERT INTO `major` VALUES ('major003', '核物理');
INSERT INTO `major` VALUES ('major004', '会计');
INSERT INTO `major` VALUES ('major005', '马克思学');
INSERT INTO `major` VALUES ('major006', '体育');
INSERT INTO `major` VALUES ('major007', '英语');
INSERT INTO `major` VALUES ('major008', '美术学');
INSERT INTO `major` VALUES ('major009', '金融学');
INSERT INTO `major` VALUES ('major010', '数学');
INSERT INTO `major` VALUES ('major011', '哲学');

-- ----------------------------
-- Table structure for person
-- ----------------------------
DROP TABLE IF EXISTS `person`;
CREATE TABLE `person`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `job_no` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `gender` varchar(1) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '男',
  `birth_day` date NULL DEFAULT NULL,
  `address` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` char(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `enrollment_year` int(11) NULL DEFAULT NULL,
  `class_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `person_type_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `remark` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `job_no`(`job_no`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of person
-- ----------------------------
INSERT INTO `person` VALUES ('017315b6446c402e8475a33f172e4d97', '2017252900', '015523', '琼奇', '女', '2003-08-20', '黑龙江大庆市', '13288789911', 2017, 'f90661529755477389909ecc0bc9bf13', 'persontype003', NULL);
INSERT INTO `person` VALUES ('035e3880e0914e8c8bef950846bf9d68', '2016219166', '7OCb00a', '宋枝', '女', '2001-09-15', '青海省海西蒙古族藏族自治州', '15152047861', 2016, 'f90661529755477389909ecc0bc9bf13', 'persontype003', NULL);
INSERT INTO `person` VALUES ('037a178758b344f2b462ec28f6a61b82', '2015167127', '878', '裘婉', '男', '2002-11-13', '黑龙江哈尔滨', '15895255274', 2015, '78d719c0a90b4199bae67a13423a69ae', 'persontype003', NULL);
INSERT INTO `person` VALUES ('039977f4d911490c9fbd4ce95a8d09db', '2020826416', '0313', '容俊滢', '男', '2000-05-02', '河北省承德市', '13905228286', 2020, '78d719c0a90b4199bae67a13423a69ae', 'persontype003', NULL);
INSERT INTO `person` VALUES ('04c6b83630534ecab4eb4ca086d3f9a8', '2019522657', '122', '阮宝梦', '男', '1999-10-21', '江西省上饶市', '15895258438', 2019, '345f0b0ef5994f06b02d9627b873516e', 'persontype003', NULL);
INSERT INTO `person` VALUES ('04fd8bb7fa664460946e439d98d98364', '2018774808', 'wS3903e', '郝颖枫', '男', '1997-05-29', '中国重庆', '15895252405', 2018, 'a5a24dfe5f1e433f9419d31dd805f2d3', 'persontype003', NULL);
INSERT INTO `person` VALUES ('0c0335c621ce40208dbff22764313f63', '2015887613', '163P1Z', '姜家辰', '男', '1998-10-05', '台湾省台北市', '15152045974', 2015, '72ba39338b9c4acd8b6a9f44963bd3ca', 'persontype003', NULL);
INSERT INTO `person` VALUES ('0c625a02b09b479ea78ca3a814912b53', '2018111545', 'FS781f3', '季良', '男', '2000-05-14', '黑龙江大庆市', '13905220638', 2018, 'f90661529755477389909ecc0bc9bf13', 'persontype003', NULL);
INSERT INTO `person` VALUES ('0df1e6f75a5741dfbdc420e64a8ba601', '2016886415', '4Am2c', '毕力健', '男', '2000-03-01', '黑龙江伊春', '15152040527', 2016, 'ea19aa4cabf64c95a4d5d17d8ee50550', 'persontype003', NULL);
INSERT INTO `person` VALUES ('0eb8bba1c29140cbbc4fffb55da08d4b', '123123', '123', '王素霞', '女', '2020-11-02', '湖北省孝感市广场街街道', '13988753765', 2016, 'class001', 'persontype003', '已辍学');
INSERT INTO `person` VALUES ('0f1cb17c2dea4d05b2131bbbc2f5060f', '0201312312', 'yyqx123', '易烊千玺', '男', '2021-01-09', '湖北省孝感市广场街街道', '13988765425', 2020, 'class001', 'persontype003', NULL);
INSERT INTO `person` VALUES ('0f4f7696ab7c4ae09c23f06914525d4f', '2016351223', '8XjZ', '平文良', '男', '2001-12-01', '江西省景德镇', '15152042112', 2016, 'class001', 'persontype003', NULL);
INSERT INTO `person` VALUES ('12459b5e2aed475eb521a2e401ee3224', '2015216924', '8345', '邢琦英', '女', '2001-04-06', '中国重庆', '15152040820', 2015, '78d719c0a90b4199bae67a13423a69ae', 'persontype003', NULL);
INSERT INTO `person` VALUES ('153012b502154146a5feaa925db305ab', '2015257660', '23347C2', '冉黛', '女', '2000-06-13', '河北省唐山市', '15895253523', 2015, '45b974a01ba14033b6197d28b2848f3e', 'persontype003', NULL);
INSERT INTO `person` VALUES ('15b301aa141e4d2eafd62db917d8988f', '2018598552', '8aTu', '周策惠', '男', '2002-01-28', '中国深圳', '13905226743', 2018, 'bd21b35a6cd141e4ac18b4c26df8f01b', 'persontype003', NULL);
INSERT INTO `person` VALUES ('15f90b4ebba24800aea8061b74aca8bc', '2020454259', 'L3skZvM', '阎炎胜', '女', '2000-09-09', '山东省青岛市', '15152048443', 2020, '12040e2ab1704d96b7029a40e6f0fee1', 'persontype003', NULL);
INSERT INTO `person` VALUES ('166a8210660c4e929d4508a8c7089956', '2018789442', '7yG4Ovr', '沸羊羊', '男', '1998-09-30', '黑龙江大庆市', '15723765345', 2018, 'f90661529755477389909ecc0bc9bf13', 'persontype003', NULL);
INSERT INTO `person` VALUES ('17eb5bf4824a420e8daa1f9d5f609944', '2019826367', 'Z583', '冉山忠', '男', '2000-03-16', '台湾省台北市', '15895255465', 2019, '9f9bb21e92674ce5a1dafbdc90a2e926', 'persontype003', NULL);
INSERT INTO `person` VALUES ('181c1fdd47394f439609bedd227d6544', '02012312312', 'wwl123', '张飞', '男', '2020-11-17', '河南省郑州市宝塔区', '18876890868', 2020, 'class001', 'persontype003', '转学籍');
INSERT INTO `person` VALUES ('19c29a4a5304440b8beb017a21c8b122', '2016143449', '34W', '薛瑶', '男', '2001-02-22', '湖北省孝感市', '15895251569', 2016, 'f90661529755477389909ecc0bc9bf13', 'persontype003', NULL);
INSERT INTO `person` VALUES ('19cfa6a15c464517bb0d2875c6c0c456', '2017454396', 'Fu8294', '林健', '女', '2000-04-11', '江西省景德镇', '15895255163', 2017, 'ea19aa4cabf64c95a4d5d17d8ee50550', 'persontype003', NULL);
INSERT INTO `person` VALUES ('1b20f6a03de74965aae9288fbb3de617', '2017715016', 'pFv3', '薛祥琬', '男', '2001-06-11', '江西省上饶市', '15895258076', 2017, '72ba39338b9c4acd8b6a9f44963bd3ca', 'persontype003', NULL);
INSERT INTO `person` VALUES ('1c1e1823ca20464d93227d7886c8e2c7', '2015553531', 'cW3M7', '尹露', '女', '1997-11-20', '台湾省台南市', '15152042920', 2015, '345f0b0ef5994f06b02d9627b873516e', 'persontype003', NULL);
INSERT INTO `person` VALUES ('1e121d66f8754e5390d550455cd4612c', '2020', '123', '奋斗小涂', '男', '1999-09-25', '湖北省武穴市', '13409828866', 2020, 'class001', 'persontype003', 'NICE');
INSERT INTO `person` VALUES ('1e79a2b5d1d74d26892dc61637b08f43', '202001201', 'abc123', '阿兹尔', '男', '2020-11-16', '英雄联盟恕瑞玛', '13444852236', 2015, 'f90661529755477389909ecc0bc9bf13', 'persontype003', '法师');
INSERT INTO `person` VALUES ('1f176326eeae4648b765789fdd0abcb5', '2020241439', '53708L', '应翔伦', '男', '1997-07-03', '中国上海', '15152048551', 2020, 'class001', 'persontype003', NULL);
INSERT INTO `person` VALUES ('216a7907372447e4b4cc41d792d4d511', '2015147283', '52m309', '黄勇', '女', '1997-03-15', '山东省临沂市', '15895259038', 2015, '9cdd656fe33245dbaff646f7d9c107df', 'persontype003', NULL);
INSERT INTO `person` VALUES ('2225201540104fb79461fab95544c99e', '2017323056', '96F', '闻新', '男', '2001-06-17', '河南省郑州市', '15895255981', 2017, '9f9bb21e92674ce5a1dafbdc90a2e926', 'persontype003', NULL);
INSERT INTO `person` VALUES ('225467725c9b4ba0acf101d16f2549fb', '2017617597', '0Vb', '穆洋辰', '女', '1999-09-23', '江西省九江市', '15152048499', 2017, 'bd21b35a6cd141e4ac18b4c26df8f01b', 'persontype003', NULL);
INSERT INTO `person` VALUES ('2419f332f8c14f6f950716fe4ec8191f', '2020777266', '9f8f2', '雷杰烟', '男', '2001-01-31', '湖北省孝感市', '13905221092', 2020, 'f90661529755477389909ecc0bc9bf13', 'persontype003', NULL);
INSERT INTO `person` VALUES ('272199acab8f4d518243da3afd299008', '2015207445', 'l5feNS', '堵钧', '男', '2003-04-17', '河南省洛阳市', '15895252330', 2015, '12040e2ab1704d96b7029a40e6f0fee1', 'persontype003', NULL);
INSERT INTO `person` VALUES ('27bd587d4b5c47e0ba56d42139ada8d7', '2019729154', '26750', '苗宝', '男', '2002-07-03', '山东省青岛市', '15152045264', 2019, '45b974a01ba14033b6197d28b2848f3e', 'persontype003', NULL);
INSERT INTO `person` VALUES ('27fc050d14b54c95a378f0b23ae06b35', '2018976280', 'U3382', '扶瑗', '女', '2001-02-10', '山东省青岛市', '13905223953', 2018, 'eca731bc0bc24c3fa158736506dd5f8a', 'persontype003', NULL);
INSERT INTO `person` VALUES ('293642195f614711b39c1ae83885cc94', '2019353914', '169ly1', '史育', '女', '2003-02-16', '湖南省岳阳市', '15152040918', 2019, 'class001', 'persontype003', NULL);
INSERT INTO `person` VALUES ('29dfcc623ee64b61b2706ae491ef1785', '2015191281', 'e5j58sD', '彭晨', '男', '2003-11-01', '江西省上饶市', '15152047888', 2015, '9cdd656fe33245dbaff646f7d9c107df', 'persontype003', NULL);
INSERT INTO `person` VALUES ('2b158ac257f04f909aea528c1603aafc', '2017661869', '5cB7474', '龚风', '女', '2001-05-09', '湖北省恩施市', '15895258838', 2017, 'class001', 'persontype003', NULL);
INSERT INTO `person` VALUES ('2b18e94d40234a06927edd36fc07c273', '2019837707', 'raC3J7', '汪柔朋', '男', '1997-01-25', '河南省洛阳市', '15895254198', 2019, '596e702819b54f89a2b3dfe9b7036386', 'persontype003', NULL);
INSERT INTO `person` VALUES ('2c44e2c01d6b4f1eb630d0ea3142fb71', '2019461922', '5107959', '卓伊心', '女', '2000-05-24', '河北省秦皇岛市', '13905220245', 2019, '1370dcf623dd4a39a05150ae9dc88d83', 'persontype003', NULL);
INSERT INTO `person` VALUES ('2d81054d65dc4ac19b5517b9e9ec8b2e', '2019309599', '6G82b2', '徐离媚豪', '女', '2000-12-21', '河南省郑州市', '15895258547', 2019, 'fb7cb4caf4474de985462172b00e96c0', 'persontype003', NULL);
INSERT INTO `person` VALUES ('2eb2e1d5cd9741fdab580394eacc7efa', '2016326435', '7050', '司安', '男', '1998-06-24', '四川省内江市', '15895254466', 2016, 'a5a24dfe5f1e433f9419d31dd805f2d3', 'persontype003', NULL);
INSERT INTO `person` VALUES ('2f94ff7d04384429b01b689989b356e4', '2015908486', '3902059', '阙瑾', '男', '2003-02-10', '黑龙江牡丹江', '15152043229', 2015, '12040e2ab1704d96b7029a40e6f0fee1', 'persontype003', NULL);
INSERT INTO `person` VALUES ('2f9c7498c20e4ef2964f5bd7bd837643', '2015634225', '146', '贾苇天', '女', '2002-12-29', '河南省郑州市', '15895258759', 2015, '9cdd656fe33245dbaff646f7d9c107df', 'persontype003', NULL);
INSERT INTO `person` VALUES ('2fa6ded0b5ff4a36beb78fc2d9fea583', 'ad', '123', '超级管理员', '男', NULL, '行政楼办公室', '13988786654', 1960, NULL, 'persontype001', NULL);
INSERT INTO `person` VALUES ('31874ccf61e14950ae53c90fa4665d19', '2015514710', 'FHDNMV2', '乔树', '女', '2001-02-14', '江西省九江市', '15895253581', 2015, '12040e2ab1704d96b7029a40e6f0fee1', 'persontype003', NULL);
INSERT INTO `person` VALUES ('31ea3003559f46da9a9bbf0516c18d38', '2020419528', 'zPd1fP6', '彭绍', '女', '2002-03-13', '江西省景德镇', '13905225302', 2020, 'class001', 'persontype003', NULL);
INSERT INTO `person` VALUES ('3300bd8d592343559cd4339f123a23e8', '2015298098', '340', '尤蝶', '男', '2000-06-13', '中国重庆', '13905220645', 2015, '12040e2ab1704d96b7029a40e6f0fee1', 'persontype003', NULL);
INSERT INTO `person` VALUES ('335bbf3e55d543f5831cfd1a53f0146a', '2018578538', 'IiyQv5', '应育菁', '男', '2000-07-14', '黑龙江牡丹江', '15152045872', 2018, '45b974a01ba14033b6197d28b2848f3e', 'persontype003', NULL);
INSERT INTO `person` VALUES ('335ce118004e42658cccfa17e6fff777', '2016962379', '67vK0', '柯嘉', '男', '1997-05-09', '山东省淄博市', '13905225697', 2016, '9f9bb21e92674ce5a1dafbdc90a2e926', 'persontype003', NULL);
INSERT INTO `person` VALUES ('3360e91f8c4c41808aa729bf0e732286', '0203214587`', 'aaabbb', '纳尔', '女', '1997-09-18', '英雄联盟班德尔城', '13866543323', 2018, '72ba39338b9c4acd8b6a9f44963bd3ca', 'persontype003', NULL);
INSERT INTO `person` VALUES ('347199abfa3842c1bc58be59d3d87eb5', '2018762306', '3c8OceE', '邵中', '女', '2003-06-20', '中国上海', '15152046357', 2018, '9f9bb21e92674ce5a1dafbdc90a2e926', 'persontype003', NULL);
INSERT INTO `person` VALUES ('351ebe0e4d11408492ead98e7cbfa21e', '2018639700', '53203', '国明冠', '男', '2000-02-14', '湖南省湘潭市', '13905224604', 2018, 'class001', 'persontype003', NULL);
INSERT INTO `person` VALUES ('3653d02c1a6e4ec3afa9e2219cc768e8', '2017474453', '50324O', '闻人仁刚', '女', '2003-02-22', '江西省九江市', '15895254100', 2017, 'a456d7f3f0ae45279c0357837a9fca3f', 'persontype003', NULL);
INSERT INTO `person` VALUES ('366a870929a5457db8bea546614467e4', '2017953367', 'Q1608f', '解羽子', '女', '1998-01-02', '四川省内江市', '15152043516', 2017, 'ea19aa4cabf64c95a4d5d17d8ee50550', 'persontype003', NULL);
INSERT INTO `person` VALUES ('373ee224dee24f09995486ec71c24f8a', '2017684286', '155679', '晏纪', '男', '2001-12-21', '台湾省高雄市', '15152041055', 2017, 'eca731bc0bc24c3fa158736506dd5f8a', 'persontype003', NULL);
INSERT INTO `person` VALUES ('37427bcf8aec49fe96d31d60ec94697e', '2020104933', 'Eb6D', '宰莎', '女', '2002-10-05', '中国深圳', '13905224520', 2020, 'bd21b35a6cd141e4ac18b4c26df8f01b', 'persontype003', NULL);
INSERT INTO `person` VALUES ('39a981c2bdfd462b8cb4dc13eeadda8f', '2015128974', 'b4g314', '景言', '男', '2003-05-13', '四川省成都市', '15895253117', 2015, '1370dcf623dd4a39a05150ae9dc88d83', 'persontype003', NULL);
INSERT INTO `person` VALUES ('39f70b172b7c4731a95fa51d551fdd22', '2019808461', '76780', '索君', '男', '1997-04-04', '山东省临沂市', '13905228004', 2019, 'f90661529755477389909ecc0bc9bf13', 'persontype003', NULL);
INSERT INTO `person` VALUES ('3a32e3fae9ac4da491a39cc0a549548c', '2015888861', 'H72rc2z', '安杰雨', '女', '1997-06-02', '湖南省长沙市', '13905227193', 2015, '596e702819b54f89a2b3dfe9b7036386', 'persontype003', NULL);
INSERT INTO `person` VALUES ('3aed31166ad149dbaa4ae7cf303d5851', '2016905921', '07j8', '刘余余', '女', '2001-08-28', '河北省秦皇岛市', '13916789965', 2016, 'class002', 'persontype003', NULL);
INSERT INTO `person` VALUES ('3b3ec37801e849f6b0e56bbc72827300', '2019404689', 'Cw5', '美羊羊', '女', '1997-04-17', '河南省洛阳市', '18834854275', 2019, 'd44c5c50b5304b819a9454dfdb130454', 'persontype003', NULL);
INSERT INTO `person` VALUES ('3c9774fb47484c1087ee828ab8dd02bd', '2016592720', '6CqqYXO', '杨晓威', '男', '1997-11-04', '黑龙江伊春', '15895258638', 2016, 'a5a24dfe5f1e433f9419d31dd805f2d3', 'persontype003', NULL);
INSERT INTO `person` VALUES ('3d14a0181cf4434b8f3f57c69c7f3ed7', '2015649236', 'u720I3y', '施卿晓', '女', '2001-06-03', '台湾省新北市', '15152043141', 2015, '345f0b0ef5994f06b02d9627b873516e', 'persontype003', NULL);
INSERT INTO `person` VALUES ('3da2467a3d1a46f8b847e932e5a41ae8', '2015339646', 'XvB5', '单雁', '女', '2000-12-08', '河北省石家庄市', '13905226104', 2015, 'eca731bc0bc24c3fa158736506dd5f8a', 'persontype003', NULL);
INSERT INTO `person` VALUES ('3deaa1fe78f14202a7dd0a65bf2e651b', '2015482045', 'l94L1', '蒋霞', '男', '1999-06-04', '中国北京', '15152047494', 2015, 'f90661529755477389909ecc0bc9bf13', 'persontype003', NULL);
INSERT INTO `person` VALUES ('3e33c205659e4e7e96005f75d0fbb389', '020321456', '111222333', '特朗普', '男', '2020-11-11', '美国白宫后院狗窝', '13577652290', 2016, 'f90661529755477389909ecc0bc9bf13', 'persontype003', '落魄总统');
INSERT INTO `person` VALUES ('3e6986f223e64da7a07ef99f724652ca', '2018879111', 'a99L', '水枫小', '男', '2001-10-23', '河南省商丘市', '15895251282', 2018, '9cdd656fe33245dbaff646f7d9c107df', 'persontype003', NULL);
INSERT INTO `person` VALUES ('3f014ee060a24a149d98f849dd1ab3ea', '2019267809', 'kp197kG', '韦美', '男', '2000-01-11', '青海省海西蒙古族藏族自治州', '13905225378', 2019, 'class001', 'persontype003', NULL);
INSERT INTO `person` VALUES ('40325d2cbcc64059987a1d31185d0bac', '2016481636', 'c227562', '甄莉斌', '男', '1998-04-13', '河南省洛阳市', '15152043086', 2016, 'eca731bc0bc24c3fa158736506dd5f8a', 'persontype003', NULL);
INSERT INTO `person` VALUES ('4080dc898ac64fcc81192687a354da01', '0203123123', 'bwl123', '霸王龙', '女', '2021-02-24', '湖北省孝感市广场街街道', '13288790043', 2016, 'class001', 'persontype003', NULL);
INSERT INTO `person` VALUES ('417dbb26ebc648c8b04b6072fb692ad8', '2017508459', '9846391', '别龙维', '男', '2001-01-23', '山东省淄博市', '13905228180', 2017, 'class001', 'persontype003', NULL);
INSERT INTO `person` VALUES ('41b96c7307454a47a93d8fc868822c25', '02021312312', 'ls123', '赵子龙', '女', '2020-11-10', '湖北省孝感市孝南区', '18864567890', 2016, 'class001', 'persontype003', NULL);
INSERT INTO `person` VALUES ('41defff4eeb24bb9b6b8bb30d2cf811f', '2015544601', 'C9jSbRj', '平明奇', '男', '2000-04-28', '黑龙江哈尔滨', '13905221842', 2015, 'eca731bc0bc24c3fa158736506dd5f8a', 'persontype003', NULL);
INSERT INTO `person` VALUES ('421ef9f476bc43d1899acf30b3f80da8', '2018525434', '4575', '广东', '男', '2002-06-04', '湖北省仙桃市', '15152044917', 2018, '72ba39338b9c4acd8b6a9f44963bd3ca', 'persontype003', NULL);
INSERT INTO `person` VALUES ('42b07a6f711c46ba89f2e4cf7ee20ee1', '2016156753', '8473888', '闻星', '男', '2000-03-10', '江西省上饶市', '15152048516', 2016, 'a5a24dfe5f1e433f9419d31dd805f2d3', 'persontype003', NULL);
INSERT INTO `person` VALUES ('42c2bed1d4ab471f84496f3433f3e38f', '2019826991', '306222', '狄彪树', '男', '2000-05-31', '湖南省衡阳市', '15152042963', 2019, '72ba39338b9c4acd8b6a9f44963bd3ca', 'persontype003', NULL);
INSERT INTO `person` VALUES ('4999d18ff9644e6ca99c1deeb42b6fcd', '02043121', 'pig123', '蓬佩奥', '男', '2020-11-12', '街头长椅', '13277895543', 2016, '72ba39338b9c4acd8b6a9f44963bd3ca', 'persontype003', '猪');
INSERT INTO `person` VALUES ('4a0119a5316248cea88b02ced075012c', '2016773569', 'w56', '封超', '女', '1997-11-16', '中国北京', '13905228993', 2016, 'd44c5c50b5304b819a9454dfdb130454', 'persontype003', NULL);
INSERT INTO `person` VALUES ('4a687af5eb3244f3b4d0e13929fe6dad', '2019178200', '2a6r', '翁柔', '女', '1999-09-29', '湖南省岳阳市', '15895253248', 2019, 'f90661529755477389909ecc0bc9bf13', 'persontype003', NULL);
INSERT INTO `person` VALUES ('4a802e6ae3c84c74b3a7bdeaa5523c52', '2017727976', '9K600', '党之绿', '女', '2003-05-05', '中国上海', '13905226391', 2017, '72ba39338b9c4acd8b6a9f44963bd3ca', 'persontype003', NULL);
INSERT INTO `person` VALUES ('4ac3578528234e5892475f70b96bfea2', '2016938748', 'D34', '贾树梵', '女', '2003-07-13', '山东省青岛市', '15895254710', 2016, '72ba39338b9c4acd8b6a9f44963bd3ca', 'persontype003', NULL);
INSERT INTO `person` VALUES ('4b38a55767da4b2e8a2e71803444e86b', '2020417485', '349', '农旭', '男', '2002-02-07', '青海省海西蒙古族藏族自治州', '13905225427', 2020, 'ea19aa4cabf64c95a4d5d17d8ee50550', 'persontype003', NULL);
INSERT INTO `person` VALUES ('4c63424c38ac44499be40b25cec7a397', '2015568054', 'RX9S733', '阙茜洋', '女', '2001-06-07', '青海省海西蒙古族藏族自治州', '15152044330', 2015, 'f90661529755477389909ecc0bc9bf13', 'persontype003', NULL);
INSERT INTO `person` VALUES ('4dcf6937f57c49bfbc9b898427759013', '2019614885', 'Y7414', '贺梵', '男', '2003-07-05', '四川省成都市', '15895253895', 2019, '9cdd656fe33245dbaff646f7d9c107df', 'persontype003', NULL);
INSERT INTO `person` VALUES ('4e2cd399c5dd496fb7715872ca6ec496', 'tc', '123', '一名资深教师', '女', '2020-11-10', '福建省福州市阳东区', '12388763382', 2006, '72ba39338b9c4acd8b6a9f44963bd3ca', 'persontype002', NULL);
INSERT INTO `person` VALUES ('514a2cfc023a45289a00cdbfcc0eb795', '2020231633', '330xq', '戚轮伯', '男', '1998-05-21', '湖北省恩施市', '15152040824', 2020, 'a456d7f3f0ae45279c0357837a9fca3f', 'persontype003', NULL);
INSERT INTO `person` VALUES ('5210a4bb8f044ea98e5f6c0afde57878', '2019926775', '2VDjh', '堵有翔', '男', '1997-04-16', '台湾省新北市', '13905229154', 2019, '1370dcf623dd4a39a05150ae9dc88d83', 'persontype003', NULL);
INSERT INTO `person` VALUES ('534279ec9f754f88af031a96d0762aec', '2019213972', '4063751', '巩致', '女', '1999-06-08', '湖北省宜昌市', '13905222696', 2019, '12040e2ab1704d96b7029a40e6f0fee1', 'persontype003', NULL);
INSERT INTO `person` VALUES ('5513467ba98744f08066d8ffffebf25a', '2019726372', '1189', '仇倩', '女', '2002-06-14', '湖南省株洲市', '15152040292', 2019, '1370dcf623dd4a39a05150ae9dc88d83', 'persontype003', NULL);
INSERT INTO `person` VALUES ('5548cfdb436f451fa9e09c2ad6832b11', '2017749854', 'afz19z6', '夏小圆', '男', '2000-02-04', '山东省济南市', '13905228662', 2017, '72ba39338b9c4acd8b6a9f44963bd3ca', 'persontype003', NULL);
INSERT INTO `person` VALUES ('5abc9b4250e0498ebcd87b7b8dc05038', '2019444826', '2jZnLv', '赵欣轮', '女', '2002-05-15', '湖北省仙桃市', '15895253428', 2019, 'eca731bc0bc24c3fa158736506dd5f8a', 'persontype003', NULL);
INSERT INTO `person` VALUES ('5bd22abe08a945028955102a296d99ae', '2015600451', '786', '孟伊', '男', '2003-12-18', '青海省海西蒙古族藏族自治州', '15895258932', 2015, 'ea19aa4cabf64c95a4d5d17d8ee50550', 'persontype003', NULL);
INSERT INTO `person` VALUES ('5c883817466f406cac8843306b8626e6', '2018808456', 'to5', '瞿彦', '女', '2001-10-03', '河北省唐山市', '13905229081', 2018, '45b974a01ba14033b6197d28b2848f3e', 'persontype003', NULL);
INSERT INTO `person` VALUES ('5d36503b737b43a0a1afbacae6a4de37', '2016381597', '9FG', '长孙固', '男', '1999-11-20', '台湾省高雄市', '13905222150', 2016, '72ba39338b9c4acd8b6a9f44963bd3ca', 'persontype003', NULL);
INSERT INTO `person` VALUES ('5f28f3e5f4ac4653b273d3202f35cb23', '2020501210', '281750', '荣嘉', '男', '2001-05-07', '湖南省衡阳市', '15895252212', 2020, 'f90661529755477389909ecc0bc9bf13', 'persontype003', NULL);
INSERT INTO `person` VALUES ('612289918c22428b99db1451cf0a7d21', '2018140066', 'Yb4', '戴涛', '女', '2000-03-06', '青海省西宁市', '13905223991', 2018, 'class002', 'persontype003', NULL);
INSERT INTO `person` VALUES ('62fec8d8585d462d8ff2632324440294', '2019735698', 'bb6', '狄莉珍', '男', '2003-07-27', '台湾省台南市', '15152049317', 2019, 'fb7cb4caf4474de985462172b00e96c0', 'persontype003', NULL);
INSERT INTO `person` VALUES ('63018f710fbf483b8d56faef14a9f7e9', '2015621750', '0499O', '印榕', '男', '2000-09-13', '黑龙江伊春', '15152043538', 2015, 'f90661529755477389909ecc0bc9bf13', 'persontype003', NULL);
INSERT INTO `person` VALUES ('6441bff3f77f41fa83a43f55abc4acd0', '2018308322', 's16674', '史锦', '男', '1998-03-02', '台湾省台北市', '15152044892', 2018, '45b974a01ba14033b6197d28b2848f3e', 'persontype003', NULL);
INSERT INTO `person` VALUES ('653f4606fdfa40ec9d62e0c476319a76', '020123213', 'qq2020520', '王维虎', '女', '2020-11-11', '河北省超级市阳光路', '12376534561', 2020, 'class001', 'persontype003', NULL);
INSERT INTO `person` VALUES ('659d8e4245684166b608b52a3af9c1e4', '2018350204', '273787', '庾朗', '女', '2003-01-24', '河南省商丘市', '15152040529', 2018, 'd44c5c50b5304b819a9454dfdb130454', 'persontype003', NULL);
INSERT INTO `person` VALUES ('65eeb8ba2d0440be9aead0408f9a56b1', '2019324294', '744', '袁雨', '女', '1998-01-17', '湖南省株洲市', '13905220252', 2019, '45b974a01ba14033b6197d28b2848f3e', 'persontype003', NULL);
INSERT INTO `person` VALUES ('679a9bfcd1394723be0c5a81fcaca204', '2020386008', 'QM4', '宇文邦芳', '女', '2001-12-17', '河北省唐山市', '15152042855', 2020, 'd44c5c50b5304b819a9454dfdb130454', 'persontype003', NULL);
INSERT INTO `person` VALUES ('6a58dc81dea04eb7ad14be45d94a5290', '2018646624', 'Q3w6', '汤玲琛', '男', '2001-01-23', '湖南省长沙市', '13905220287', 2018, 'ea19aa4cabf64c95a4d5d17d8ee50550', 'persontype003', NULL);
INSERT INTO `person` VALUES ('6bb6635bd9024712b449790ab85eb806', '2020747776', 'w40b76', '樊贤中', '男', '1998-02-03', '江西省九江市', '13905227074', 2020, '45b974a01ba14033b6197d28b2848f3e', 'persontype003', NULL);
INSERT INTO `person` VALUES ('6bcbbaeb13f542d4ad692a1bc03b98f6', '2018235887', 'V0X', '王婷', '女', '2002-06-14', '山东省济南市', '15152044564', 2018, '596e702819b54f89a2b3dfe9b7036386', 'persontype003', NULL);
INSERT INTO `person` VALUES ('6c0d99b0a1224162ae447c2a0d672054', '2017346153', 'o959', '禄桦萍', '男', '1999-03-06', '青海省西宁市', '13905226087', 2017, '45b974a01ba14033b6197d28b2848f3e', 'persontype003', NULL);
INSERT INTO `person` VALUES ('7222cf50937644f084e2bbd68de9582d', '2016261964', 'tUDfD5', '刘祖全', '男', '2002-06-05', '河南省商丘市', '17671852171', 2016, '45b974a01ba14033b6197d28b2848f3e', 'persontype003', NULL);
INSERT INTO `person` VALUES ('72354604b6ea48268a540781ce04f5d4', '2020603151', '2h8W', '懒洋洋', '男', '2000-12-02', '河南省许昌市', '17843245629', 2020, '1370dcf623dd4a39a05150ae9dc88d83', 'persontype003', NULL);
INSERT INTO `person` VALUES ('724f6320d2434ff29f2eb1215fb4b58c', '2018273427', 'c9V97M', '凌彬承', '女', '2002-12-11', '山东省临沂市', '15895259392', 2018, '345f0b0ef5994f06b02d9627b873516e', 'persontype003', NULL);
INSERT INTO `person` VALUES ('72a763969dae4d298c7f317fee2ac9da', '2020460083', 'LaqPU2', '甄华', '男', '2001-02-14', '湖南省长沙市', '15152049354', 2020, '78d719c0a90b4199bae67a13423a69ae', 'persontype003', NULL);
INSERT INTO `person` VALUES ('73184e2ccf684fed854a79e9c08c713c', '3123', 'zbs1314', '赵本山', '女', '2020-11-10', '湖北省孝感市广场街街道', '12345387963', 2016, 'class001', 'persontype003', NULL);
INSERT INTO `person` VALUES ('736b612b18c4418880fc918dd8d2bbaa', '02012312', '123', '橘右京', '男', '2020-11-30', '王者荣耀日本动漫', '12388990011', 2018, '72ba39338b9c4acd8b6a9f44963bd3ca', 'persontype003', NULL);
INSERT INTO `person` VALUES ('74886d5baf7346cbbacbf6c2aef534fe', '2016428914', '25022', '景彦荔', '女', '1998-10-05', '江西省南昌市', '15895252961', 2016, 'a456d7f3f0ae45279c0357837a9fca3f', 'persontype003', NULL);
INSERT INTO `person` VALUES ('754d00325b8f48e3a170eb157b97405f', '2015296150', '07y567', '魏逸志', '女', '2001-08-14', '中国北京', '15895252802', 2015, '78d719c0a90b4199bae67a13423a69ae', 'persontype003', NULL);
INSERT INTO `person` VALUES ('75c57b71cac54f3c9c9c68e68429ebae', '2020539658', 'dJ8uP6E', '汪威贝', '女', '2000-09-15', '青海省玉树藏族自治州', '15895255121', 2020, 'd44c5c50b5304b819a9454dfdb130454', 'persontype003', NULL);
INSERT INTO `person` VALUES ('76de044b4a49438db8d36e2f91366652', '2018524331', '1975KRy', '卫妍诚', '女', '1997-08-09', '黑龙江伊春', '15895253722', 2018, '345f0b0ef5994f06b02d9627b873516e', 'persontype003', NULL);
INSERT INTO `person` VALUES ('78f93d94c9144b25869391fd4f6f167b', '2020162110', '17T1p', '黎琪羽', '女', '1997-06-16', '山东省济南市', '15152045954', 2020, '12040e2ab1704d96b7029a40e6f0fee1', 'persontype003', NULL);
INSERT INTO `person` VALUES ('791cbb050186417d9c63f4b67e7c46d0', '2020358841', '9404', '索生俊', '女', '1997-11-09', '中国北京', '15152047114', 2020, '78d719c0a90b4199bae67a13423a69ae', 'persontype003', NULL);
INSERT INTO `person` VALUES ('796263db9edf46f0a5526036e997fff3', '2016657375', '8vT9F57', '吕成', '男', '2000-08-10', '河北省秦皇岛市', '15152046303', 2016, '9f9bb21e92674ce5a1dafbdc90a2e926', 'persontype003', NULL);
INSERT INTO `person` VALUES ('7aba308ecee74c0d97f0740ab103e928', '2016268517', '01A', '雷飘全', '男', '1997-05-14', '台湾省台北市', '15152048210', 2016, '1370dcf623dd4a39a05150ae9dc88d83', 'persontype003', NULL);
INSERT INTO `person` VALUES ('7ac5de6b9bd848e8957b67f05d388fa8', '2020106655', 'RcH4OB0', '方紫', '女', '2002-04-05', '河北省秦皇岛市', '13905226899', 2020, 'a456d7f3f0ae45279c0357837a9fca3f', 'persontype003', NULL);
INSERT INTO `person` VALUES ('7ae66482fc5f43e397dc03e491cc1af6', '2018428743', '12M3R65', '金刚萍', '男', '1998-02-25', '河北省唐山市', '15152046462', 2018, 'class002', 'persontype003', NULL);
INSERT INTO `person` VALUES ('7b81ef6eeb87439f85a6b966348eca9f', '2015712165', 'n2kka', '胥俊悦', '女', '1999-01-16', '黑龙江大庆市', '15895255332', 2015, 'd44c5c50b5304b819a9454dfdb130454', 'persontype003', NULL);
INSERT INTO `person` VALUES ('7c5ce9d217fa46b4b39b777b45be7d8c', '2016852726', '68450', '虞谦刚', '女', '1998-03-28', '江西省景德镇', '13905221368', 2016, '45b974a01ba14033b6197d28b2848f3e', 'persontype003', NULL);
INSERT INTO `person` VALUES ('7c8c266cbf4740c88a623e7c7b6458b2', '2020247027', 'tz3D', '澹台珊', '女', '2003-08-02', '黑龙江牡丹江', '15895256390', 2020, '45b974a01ba14033b6197d28b2848f3e', 'persontype003', NULL);
INSERT INTO `person` VALUES ('80087a7ed5f1462f931b7846ccecec2c', '2016742890', '328', '舒炎厚', '男', '1999-10-14', '河北省唐山市', '15152041570', 2016, '72ba39338b9c4acd8b6a9f44963bd3ca', 'persontype003', NULL);
INSERT INTO `person` VALUES ('8284c5cb2fd943ac9284a0e4732d5d88', '2017598290', '01i8dJ0', '通青苇', '女', '2003-09-29', '台湾省台北市', '13905223869', 2017, 'a5a24dfe5f1e433f9419d31dd805f2d3', 'persontype003', NULL);
INSERT INTO `person` VALUES ('82a1dd18368549beb5013e0e2076ad74', '2020397299', 'pv0Y9dy', '郑寒之', '男', '2003-12-11', '湖北省仙桃市', '15152045613', 2020, 'class002', 'persontype003', NULL);
INSERT INTO `person` VALUES ('845ac951772d4e88afb9fa14f7f7b526', '2017364636', 'L67d', '通纨', '女', '2000-01-25', '湖北省武汉市', '13905228066', 2017, '72ba39338b9c4acd8b6a9f44963bd3ca', 'persontype003', NULL);
INSERT INTO `person` VALUES ('84bf4bff04714350ba0154bb0c803cd4', '2019963506', '90294W', '洪壮', '男', '1999-07-03', '江西省景德镇', '13905229870', 2019, '78d719c0a90b4199bae67a13423a69ae', 'persontype003', NULL);
INSERT INTO `person` VALUES ('8528f212a25047e3a51a9b2ecb15ae13', '2019383267', '96Nyg', '柯胜仪', '女', '2001-07-07', '台湾省台南市', '15152049649', 2019, 'd44c5c50b5304b819a9454dfdb130454', 'persontype003', NULL);
INSERT INTO `person` VALUES ('857278afcb424e479be4305e474bad66', '2019883159', 'Y291WS7', '奚谦', '男', '2000-01-07', '湖南省湘潭市', '15152047276', 2019, 'd44c5c50b5304b819a9454dfdb130454', 'persontype003', NULL);
INSERT INTO `person` VALUES ('859628b42d7d4dac9dbbe943ea38e3c6', '2017197868', '16M1N', '易晴冰', '女', '2001-09-06', '台湾省台南市', '15895250627', 2017, '45b974a01ba14033b6197d28b2848f3e', 'persontype003', NULL);
INSERT INTO `person` VALUES ('86433367a1ae439a9558a5c6ee1ae269', '2015255902', '9RL', '韩剑时', '男', '1998-01-07', '中国上海', '13905228012', 2015, '78d719c0a90b4199bae67a13423a69ae', 'persontype003', NULL);
INSERT INTO `person` VALUES ('89ecdee85c2849118c29ad247f5b5204', '2020123996', '12282', '舒宏', '女', '2001-07-03', '河南省郑州市', '13905228754', 2020, 'f90661529755477389909ecc0bc9bf13', 'persontype003', NULL);
INSERT INTO `person` VALUES ('8a8bbea91936442eb71f055354052b47', '2015109597', '53431', '宁风', '女', '1998-10-05', '青海省玉树藏族自治州', '13905227204', 2015, '345f0b0ef5994f06b02d9627b873516e', 'persontype003', NULL);
INSERT INTO `person` VALUES ('8ac387c3ddca464d8893487530693aae', '2019998843', '200', '昌芳', '女', '1999-06-06', '台湾省台中市', '13905223357', 2019, 'class001', 'persontype003', NULL);
INSERT INTO `person` VALUES ('8b9817fea11c4361a10d919b344b373f', '2015200423', '87785', '甄佳', '女', '1998-07-15', '湖北省恩施市', '15152043345', 2015, 'a5a24dfe5f1e433f9419d31dd805f2d3', 'persontype003', NULL);
INSERT INTO `person` VALUES ('8bcc3b347d1b4f16b3bdc85ba06fd1b6', '2020137437', '2olkp', '武风', '男', '2000-08-30', '四川省南充市', '13905223422', 2020, '78d719c0a90b4199bae67a13423a69ae', 'persontype003', NULL);
INSERT INTO `person` VALUES ('9157461e00dc4886b893060cc425119d', '2017945508', '803937', '屠瑶', '女', '2000-04-01', '中国上海', '15895253798', 2017, '596e702819b54f89a2b3dfe9b7036386', 'persontype003', NULL);
INSERT INTO `person` VALUES ('93585ab4087e46d7b41e7a95bdbda6e5', '2017467384', '8325729', '许恒', '男', '2000-06-16', '湖南省株洲市', '13905220381', 2017, '78d719c0a90b4199bae67a13423a69ae', 'persontype003', NULL);
INSERT INTO `person` VALUES ('946223747d224ccd9cdb66f72112c731', '2020239725', '9u2Xp', '伏佳', '女', '1997-12-29', '湖南省邵阳市', '15152042377', 2020, '9cdd656fe33245dbaff646f7d9c107df', 'persontype003', NULL);
INSERT INTO `person` VALUES ('981798ebb61d42e09cdd5b27bb602be8', '2019939152', '53W127', '贡毓洋', '女', '1999-12-16', '中国重庆', '15152049418', 2019, 'a456d7f3f0ae45279c0357837a9fca3f', 'persontype003', NULL);
INSERT INTO `person` VALUES ('99107fc5f6ee493dbb59e47aee4ae38d', '2015710668', 'y28', '元逸婕', '男', '1998-02-12', '黑龙江伊春', '13905226222', 2015, '9f9bb21e92674ce5a1dafbdc90a2e926', 'persontype003', NULL);
INSERT INTO `person` VALUES ('99227df964b846c7b6a5b12a4c9dc04c', '2015899079', '172530g', '宰影真', '男', '1999-09-20', '山东省济南市', '15895252982', 2015, 'f90661529755477389909ecc0bc9bf13', 'persontype003', NULL);
INSERT INTO `person` VALUES ('998c45dfa8f44c3f86fa46dab93e532c', '2016992900', '11R04', '封丹怡', '男', '1999-02-04', '中国深圳', '13905228343', 2016, '9f9bb21e92674ce5a1dafbdc90a2e926', 'persontype003', NULL);
INSERT INTO `person` VALUES ('99cc3cae5d26404bb5c78cd7ed7078c3', '2018997466', '649684', '云思', '女', '1999-07-23', '河南省郑州市', '13905228680', 2018, '78d719c0a90b4199bae67a13423a69ae', 'persontype003', NULL);
INSERT INTO `person` VALUES ('9af4772b8df24306940e4d19578d759d', '2019132660', 's36cg2', '包玲蓓', '男', '2001-07-03', '山东省淄博市', '15152045430', 2019, 'class001', 'persontype003', NULL);
INSERT INTO `person` VALUES ('9d8cfc141ce740a0b913ebd834525afa', '2015178680', '9267p', '葛贝', '男', '1998-07-15', '河北省唐山市', '15152046502', 2015, '9cdd656fe33245dbaff646f7d9c107df', 'persontype003', NULL);
INSERT INTO `person` VALUES ('9dbfa8d3cf404a9d9fbf42df2a33ec4f', '2020483847', 'T7x', '花莺', '女', '1998-10-11', '中国深圳', '15895258648', 2020, 'a456d7f3f0ae45279c0357837a9fca3f', 'persontype003', NULL);
INSERT INTO `person` VALUES ('9efb76e8f0074651a9ddde23a08b75ef', '2016182419', '2k46', '贺珊', '女', '1998-08-21', '四川省成都市', '13905228612', 2016, '12040e2ab1704d96b7029a40e6f0fee1', 'persontype003', NULL);
INSERT INTO `person` VALUES ('9f263676386c42bc815e7bc6b94c191d', '2015132352', 'E37sJ89', '黄菊', '女', '2000-05-21', '台湾省台南市', '15895252795', 2015, 'class001', 'persontype003', NULL);
INSERT INTO `person` VALUES ('a043077da6914b128acf93303b3f7cd9', '2018353035', 'I2HqU7', '寇辰凤', '男', '2000-02-15', '台湾省台中市', '15895254315', 2018, '1370dcf623dd4a39a05150ae9dc88d83', 'persontype003', NULL);
INSERT INTO `person` VALUES ('a0ac5e603de54fce8153fd003683b507', '2018432553', 'n6XC6', '刘进瑾', '女', '1997-02-10', '中国重庆', '15152042366', 2018, '78d719c0a90b4199bae67a13423a69ae', 'persontype003', NULL);
INSERT INTO `person` VALUES ('a0bb8422130743b79d12e5a60fc8e5a1', '2017409479', '7761', '云珠林', '女', '2000-07-05', '河南省许昌市', '13905228113', 2017, 'ea19aa4cabf64c95a4d5d17d8ee50550', 'persontype003', NULL);
INSERT INTO `person` VALUES ('a248851f505d4bbab36825427fcb0ea6', '2015705385', 'cl8izGQ', '庄毅武', '男', '2000-01-16', '江西省上饶市', '15152043430', 2015, 'class002', 'persontype003', NULL);
INSERT INTO `person` VALUES ('a2ac87583db046529cbd32b416e64747', '2020524062', '176', '宇文逸子', '女', '2002-07-24', '台湾省新北市', '15895259569', 2020, 'ea19aa4cabf64c95a4d5d17d8ee50550', 'persontype003', NULL);
INSERT INTO `person` VALUES ('a509d3466ca64c55b6f1d66b48383711', '2015959422', '90v', '田琪雅', '男', '2001-08-13', '河北省秦皇岛市', '13905227185', 2015, 'class002', 'persontype003', NULL);
INSERT INTO `person` VALUES ('a5d69901968f43898f79a3b548941dfe', '2019406986', '7qB76', '荣荣育', '男', '2001-02-27', '山东省临沂市', '15152044590', 2019, 'a5a24dfe5f1e433f9419d31dd805f2d3', 'persontype003', NULL);
INSERT INTO `person` VALUES ('a6d330da1f664389afdcb2c1f87bddbd', '2019563788', '407j07', '农建', '男', '2003-02-27', '河南省洛阳市', '15895258958', 2019, 'bd21b35a6cd141e4ac18b4c26df8f01b', 'persontype003', NULL);
INSERT INTO `person` VALUES ('a79d6a16cdce428b94bd9343dca18673', '2017641506', 'adbr3T', '阙姬', '男', '2001-11-14', '河北省唐山市', '13905227308', 2017, '9f9bb21e92674ce5a1dafbdc90a2e926', 'persontype003', NULL);
INSERT INTO `person` VALUES ('a9a9d12fe8bc442f9fcaed041c2417b2', '2015711403', '027', '丁志', '女', '1997-08-11', '山东省济南市', '15895250669', 2015, '72ba39338b9c4acd8b6a9f44963bd3ca', 'persontype003', NULL);
INSERT INTO `person` VALUES ('a9bbab83fbe042839dce6b460c2736aa', '2020763800', 'VduSX6', '虞婉轩', '男', '2000-10-25', '河北省石家庄市', '15895254541', 2020, 'eca731bc0bc24c3fa158736506dd5f8a', 'persontype003', NULL);
INSERT INTO `person` VALUES ('a9d7af7898084268b3104a7ee8d38025', '2015163744', '7j2585', '黎奇', '女', '2000-06-11', '河南省洛阳市', '15895251693', 2015, '78d719c0a90b4199bae67a13423a69ae', 'persontype003', NULL);
INSERT INTO `person` VALUES ('aa295e3049c64b01b0f0561bb4b966a5', '2019600789', '8p3941', '谈胜世', '女', '2001-04-23', '河南省洛阳市', '15895253913', 2019, '78d719c0a90b4199bae67a13423a69ae', 'persontype003', NULL);
INSERT INTO `person` VALUES ('acf28183bab641aaa3b0b2e2f4f31ab1', '2015775851', '606', '闻晨', '男', '2003-01-15', '河南省许昌市', '13905222160', 2015, 'a5a24dfe5f1e433f9419d31dd805f2d3', 'persontype003', NULL);
INSERT INTO `person` VALUES ('ad2c82a0fffe43219b03095b240f25f0', '2017754773', '1A976', '郎岩', '女', '2001-02-06', '黑龙江哈尔滨', '15895251997', 2017, 'f90661529755477389909ecc0bc9bf13', 'persontype003', NULL);
INSERT INTO `person` VALUES ('ad41b9b8be3a4dddb8291636e30117d9', '2018847498', '445P8', '严堂', '女', '2000-07-28', '四川省内江市', '13905223704', 2018, 'f90661529755477389909ecc0bc9bf13', 'persontype003', NULL);
INSERT INTO `person` VALUES ('ae7d7650de5a49448fa48c502465de9f', '2018447712', '2867', '甄灵绿', '女', '2003-12-18', '青海省西宁市', '15152046648', 2018, 'class001', 'persontype003', NULL);
INSERT INTO `person` VALUES ('ae9a9a8f62554ca5be453dd93ba2e48c', '2015635679', 'vWZNh9W', '仇星勇', '女', '2003-06-07', '湖北省黄冈市', '15152040352', 2015, 'class001', 'persontype003', NULL);
INSERT INTO `person` VALUES ('af4e485209634e60af3b98eab9450e5a', '2020365227', '55447', '单于秋', '女', '2003-02-13', '湖南省长沙市', '13905224495', 2020, '596e702819b54f89a2b3dfe9b7036386', 'persontype003', NULL);
INSERT INTO `person` VALUES ('af9862e728f2454085c804e0a38896d0', '2015810157', '03Jsr4u', '孔克思', '女', '2001-04-22', '台湾省台中市', '15895255491', 2015, '1370dcf623dd4a39a05150ae9dc88d83', 'persontype003', NULL);
INSERT INTO `person` VALUES ('b52980e30fd24165aa010e52d9788f3a', '2016855849', 'P84Z0', '耿辉翠', '男', '2003-09-14', '四川省广安市', '15895258873', 2016, 'd44c5c50b5304b819a9454dfdb130454', 'persontype003', NULL);
INSERT INTO `person` VALUES ('b5bee299ffbe40eab847b8c9644fff91', '020321412', '123123', '卡尔萨斯', '女', '2020-11-30', '暗影岛', '13576307702', 2018, 'ea19aa4cabf64c95a4d5d17d8ee50550', 'persontype003', NULL);
INSERT INTO `person` VALUES ('b757d0281b5845aea6480ec7ce0b2a2d', '2019664584', '1I8648', '萧舒敬', '男', '2002-12-15', '黑龙江鹤岗市', '15895256886', 2019, 'a456d7f3f0ae45279c0357837a9fca3f', 'persontype003', NULL);
INSERT INTO `person` VALUES ('b8073915ed45405583a34ed3c53e6a0b', '2017575762', 'Zkp8', '尹翠志', '男', '2003-08-04', '四川省乐山市', '15152047088', 2017, 'f90661529755477389909ecc0bc9bf13', 'persontype003', NULL);
INSERT INTO `person` VALUES ('b8882543970f4a87859a18bd03fd59f1', '2020508615', '0Pb2NM', '贾小露', '男', '1997-07-03', '黑龙江哈尔滨', '13905225880', 2020, 'd44c5c50b5304b819a9454dfdb130454', 'persontype003', NULL);
INSERT INTO `person` VALUES ('bc0482e0805a43438fc60d7cffb99783', '2020980058', '1238', '公羊飞', '男', '1999-10-26', '河北省承德市', '15152047096', 2020, '9cdd656fe33245dbaff646f7d9c107df', 'persontype003', NULL);
INSERT INTO `person` VALUES ('be5a58a5f243463fab0fa4dcf61d0876', '2016337091', '477837', '昌若', '男', '1999-05-01', '江西省上饶市', '13905223398', 2016, 'a456d7f3f0ae45279c0357837a9fca3f', 'persontype003', NULL);
INSERT INTO `person` VALUES ('c09c06f6768b42479f7f03d3ee4d9b24', '2015717333', '2300', '劳卿婉', '女', '2003-06-14', '江西省上饶市', '15895258400', 2015, 'bd21b35a6cd141e4ac18b4c26df8f01b', 'persontype003', NULL);
INSERT INTO `person` VALUES ('c0f0f05675df4cdd80d3035a4482f5c8', '02032125', 'biden520', '拜登', '男', '1983-11-30', '美国白宫', '12311234456', 2020, 'bd21b35a6cd141e4ac18b4c26df8f01b', 'persontype003', '现任美国总统');
INSERT INTO `person` VALUES ('c4bc49934c954efabc6bc62826c60c7d', '2016250525', '435', '东弘', '男', '1998-11-14', '台湾省新北市', '15895252770', 2016, '345f0b0ef5994f06b02d9627b873516e', 'persontype003', NULL);
INSERT INTO `person` VALUES ('c65665ddda1f4f08ae9cbf61a2db5968', '020321722', 'iloveyou', '陈国志', '男', '1980-11-20', '湖北省孝感市广场小区', '12977543326', 2000, NULL, 'persontype002', NULL);
INSERT INTO `person` VALUES ('c8b492d5222c4f668fc63dfc8ab6d1e1', '2015170817', 'zr9gs', '罗清', '女', '2003-08-07', '江西省景德镇', '13905226187', 2015, 'f90661529755477389909ecc0bc9bf13', 'persontype003', NULL);
INSERT INTO `person` VALUES ('c90edffa6ed149f894c0fef2b8f3b555', '020123123', 'yl123', '王俊凯', '女', '2020-11-09', '北京市东城区', '12321312312', 2019, 'class001', 'persontype003', NULL);
INSERT INTO `person` VALUES ('c9337e59bb3d4827a22d2178a77cdd49', '2019329819', '840c875', '寿雨', '女', '2001-07-17', '中国北京', '15895254663', 2019, '9f9bb21e92674ce5a1dafbdc90a2e926', 'persontype003', NULL);
INSERT INTO `person` VALUES ('cab0e11f9b7d407795349069b6b6ff1d', '2018185194', 'r45', '黄婷', '男', '1999-08-14', '中国重庆', '15895250028', 2018, '1370dcf623dd4a39a05150ae9dc88d83', 'persontype003', NULL);
INSERT INTO `person` VALUES ('cb5fe2a3eee54ec48491b96465339942', '2015322729', '2133Q', '邓哲恒', '男', '2003-09-08', '山东省淄博市', '13905222353', 2015, '345f0b0ef5994f06b02d9627b873516e', 'persontype003', NULL);
INSERT INTO `person` VALUES ('cc99fd6352c144a6954719d340d5425e', '2020516878', '060', '李滢', '女', '2002-07-16', '四川省雅安市', '15895258255', 2020, 'd44c5c50b5304b819a9454dfdb130454', 'persontype003', NULL);
INSERT INTO `person` VALUES ('ccca7b19d76f4312bc85d907a6f4142e', '2019344472', '6t3kp5', '常善勤', '女', '1998-04-16', '青海省西宁市', '15152040617', 2019, '596e702819b54f89a2b3dfe9b7036386', 'persontype003', NULL);
INSERT INTO `person` VALUES ('d230397fe58a43bd947752a02a3d2fe5', '2015128526', '4ci', '贺卿', '男', '1997-09-11', '四川省广安市', '15895257677', 2015, '9cdd656fe33245dbaff646f7d9c107df', 'persontype003', NULL);
INSERT INTO `person` VALUES ('d2348681cf86485cb2bf5bc53aca4f51', '2020150836', 'E1aL', '许峰', '女', '2000-01-25', '山东省济南市', '13905227986', 2020, 'a5a24dfe5f1e433f9419d31dd805f2d3', 'persontype003', NULL);
INSERT INTO `person` VALUES ('d44f39c9e6f84f70b37cad6e85bcf9a2', '2019372259', '1qK', '管珍', '女', '2001-01-26', '湖北省黄冈市', '15895257015', 2019, 'a456d7f3f0ae45279c0357837a9fca3f', 'persontype003', NULL);
INSERT INTO `person` VALUES ('d4505175c905475eac7965216a24d6d9', '2017138233', '295', '喜洋洋', '男', '2000-02-12', '青海省玉树藏族自治州', '13942342123', 2017, '1370dcf623dd4a39a05150ae9dc88d83', 'persontype003', NULL);
INSERT INTO `person` VALUES ('d4adcdae22a54c3f8def2c8b4f12754e', '2015441750', '730I', '柯琼彦', '女', '2001-05-16', '湖北省黄冈市', '15895258332', 2015, 'class001', 'persontype003', NULL);
INSERT INTO `person` VALUES ('d4e2c49eb8a74c9eba64f8e405f2bc75', '2018662215', '2148255', '朱琼盛', '女', '2001-10-22', '中国深圳', '15895259200', 2018, 'class001', 'persontype003', NULL);
INSERT INTO `person` VALUES ('d9172622b26243e8a5cec52f8fb589b4', '2019874078', '898', '郭桦', '女', '2003-11-24', '中国北京', '15152048942', 2019, 'ea19aa4cabf64c95a4d5d17d8ee50550', 'persontype003', NULL);
INSERT INTO `person` VALUES ('d95bd29a92874a17a8a718c6cc2f1969', '2016720737', '270', '汤琪', '男', '2003-05-26', '台湾省台北市', '13905223683', 2016, 'd44c5c50b5304b819a9454dfdb130454', 'persontype003', NULL);
INSERT INTO `person` VALUES ('db35cb341ed744868a1ddd8012a6a5d5', '2015407650', 'X6gx', '东芬全', '女', '1997-02-17', '山东省青岛市', '15152041882', 2015, 'f90661529755477389909ecc0bc9bf13', 'persontype003', NULL);
INSERT INTO `person` VALUES ('dd58100fc3b44fdc9f194710342c8dad', '2018755870', 'U7s2AK', '方华', '女', '2001-12-19', '湖南省株洲市', '15895256941', 2018, 'a456d7f3f0ae45279c0357837a9fca3f', 'persontype003', NULL);
INSERT INTO `person` VALUES ('df54db35c86646dabf6eb321252a5cbe', '2020437552', '36928', '平璐', '女', '2001-02-05', '湖北省孝感市', '15895254648', 2020, 'class001', 'persontype003', NULL);
INSERT INTO `person` VALUES ('dff267f6f2bb4251a3bc2ad864b77eb2', '2015841458', '969', '韦昭', '男', '2000-03-22', '河北省唐山市', '15152040954', 2015, '72ba39338b9c4acd8b6a9f44963bd3ca', 'persontype003', NULL);
INSERT INTO `person` VALUES ('e110acbcae2543a1ac4d2fee0141303b', '2016868634', '3J64', '孔琬', '男', '1999-09-01', '青海省海西蒙古族藏族自治州', '15152042387', 2016, '1370dcf623dd4a39a05150ae9dc88d83', 'persontype003', NULL);
INSERT INTO `person` VALUES ('e12abe5f89aa40e7b25d5bda0d24af76', '2020966248', '76OKpn', '甘剑', '男', '2003-08-29', '湖北省孝感市', '13905226706', 2020, 'bd21b35a6cd141e4ac18b4c26df8f01b', 'persontype003', NULL);
INSERT INTO `person` VALUES ('e1e9d2327cfe487d833f315caddd4b21', '2015129875', 'M335', '耿仪澜', '男', '1999-07-10', '湖北省武汉市', '15152041161', 2015, '9f9bb21e92674ce5a1dafbdc90a2e926', 'persontype003', NULL);
INSERT INTO `person` VALUES ('e9694909521a4a25bdd552fa6b3f10ed', '2015681709', '156', '刘长生', '男', '2002-08-04', '台湾省新北市', '13216789965', 2015, '9cdd656fe33245dbaff646f7d9c107df', 'persontype003', NULL);
INSERT INTO `person` VALUES ('ea6adf1cf19d4fa7a52c529953701a91', '2016951592', '62e5g2', '孟琬', '女', '1999-08-17', '青海省玉树藏族自治州', '15895254609', 2016, '9cdd656fe33245dbaff646f7d9c107df', 'persontype003', NULL);
INSERT INTO `person` VALUES ('ec4bca847f60444cb35d94becd3f345a', '2018779717', 'vsh68', '缪妮莺', '男', '1998-12-16', '河南省商丘市', '15895253723', 2018, '78d719c0a90b4199bae67a13423a69ae', 'persontype003', NULL);
INSERT INTO `person` VALUES ('ed2d8c606dd640db9ae9cd8843f79bef', '2018960593', '614', '邵剑', '女', '2000-01-05', '河北省石家庄市', '15895250006', 2018, 'bd21b35a6cd141e4ac18b4c26df8f01b', 'persontype003', NULL);
INSERT INTO `person` VALUES ('ede5d5ee0e434c1e88a37885323b3312', '02032187', '123', '娜可露露', '女', '1998-11-30', '王者荣耀日本动漫', '13566742211', 2017, '72ba39338b9c4acd8b6a9f44963bd3ca', 'persontype003', NULL);
INSERT INTO `person` VALUES ('ee877838ed00478bbc27edfc64030617', '2020349131', 'W9F3', '吉翠', '男', '1997-10-19', '中国重庆', '15895257565', 2020, '9f9bb21e92674ce5a1dafbdc90a2e926', 'persontype003', NULL);
INSERT INTO `person` VALUES ('eebc767825ad40d1b0700b886fd44504', '2017977263', '8CT6', '景瑾嘉', '女', '2000-02-20', '江西省景德镇', '15152046560', 2017, '12040e2ab1704d96b7029a40e6f0fee1', 'persontype003', NULL);
INSERT INTO `person` VALUES ('ef0efedf814043258df424f2cd7fc839', '2020719747', '049636', '都诚', '女', '2000-11-22', '青海省西宁市', '15152043993', 2020, '72ba39338b9c4acd8b6a9f44963bd3ca', 'persontype003', NULL);
INSERT INTO `person` VALUES ('efa8276fe9674c59b84a8a43447eda9c', '2020367140', '5797566', '柯厚', '男', '2001-06-13', '湖北省宜昌市', '15895250462', 2020, '9f9bb21e92674ce5a1dafbdc90a2e926', 'persontype003', NULL);
INSERT INTO `person` VALUES ('f19b8ab9001745129e9129c45509ab5c', '2020924633', 'Gi32n9S', '宰成蕊', '女', '2003-06-07', '河北省秦皇岛市', '15895253819', 2020, 'fb7cb4caf4474de985462172b00e96c0', 'persontype003', NULL);
INSERT INTO `person` VALUES ('f28b9c90913649baa0a4620cc712da55', '2020412851', 'LQOLdh9', '向兴', '女', '1998-03-01', '河南省许昌市', '15152042262', 2020, '78d719c0a90b4199bae67a13423a69ae', 'persontype003', NULL);
INSERT INTO `person` VALUES ('f4593ec0dfba4b97a859962062f9dbd7', '2020817268', '73845y1', '屠刚康', '男', '2000-05-01', '河南省洛阳市', '13905226150', 2020, '45b974a01ba14033b6197d28b2848f3e', 'persontype003', NULL);
INSERT INTO `person` VALUES ('f69d449fff3c4f9dbc448f05b6fec33f', '2018289098', '60H', '花娅竹', '男', '2001-08-19', '四川省南充市', '13905223815', 2018, 'd44c5c50b5304b819a9454dfdb130454', 'persontype003', NULL);
INSERT INTO `person` VALUES ('f96d008d09c340ecbbf8cb549bca81c5', '2016112674', 'L78', '万苛楠', '女', '2001-10-23', '山东省临沂市', '15152041679', 2016, 'class001', 'persontype003', NULL);
INSERT INTO `person` VALUES ('fc5a94505bf34f55ae56a7ef17a7c6f0', '2015935858', 'IMFKv7', '柳眉', '男', '2002-02-05', '青海省海西蒙古族藏族自治州', '13905228804', 2015, '72ba39338b9c4acd8b6a9f44963bd3ca', 'persontype003', NULL);
INSERT INTO `person` VALUES ('fda73349f4cb40a1bfc6ef581d133ffc', '2015916714', 'l4CQre', '杭健雯', '男', '1997-04-05', '四川省广安市', '15152047398', 2015, '9f9bb21e92674ce5a1dafbdc90a2e926', 'persontype003', NULL);
INSERT INTO `person` VALUES ('fde52afcd58d4374aace446adf88b49d', '2015702685', 'DZ1maI', '薛薇凡', '男', '2001-09-09', '中国上海', '15895252784', 2015, 'd44c5c50b5304b819a9454dfdb130454', 'persontype003', NULL);
INSERT INTO `person` VALUES ('ff5b36414c65481ab7df6b66d9b7dde6', '2018946331', '78q70', '弘娟士', '女', '1998-01-06', '台湾省台南市', '13905228209', 2018, '12040e2ab1704d96b7029a40e6f0fee1', 'persontype003', NULL);
INSERT INTO `person` VALUES ('fffa7f79c0a642c383dee0f88eb7a6a6', '2019811669', 'z4lh', '邓苛桂', '男', '2000-07-18', '湖北省孝感市', '13905221667', 2019, 'eca731bc0bc24c3fa158736506dd5f8a', 'persontype003', NULL);
INSERT INTO `person` VALUES ('stu001', '0203217', '123', '吴先飞', '女', '2020-11-06', '湖北省孝感市广场街街道', '13822334455', 2019, 'class002', 'persontype003', '于2020年参军入伍');
INSERT INTO `person` VALUES ('stu002', '20200320318', 'aaa123', '刘晓倩', '女', '1999-03-19', '湖北省抚州市自强', '18877216673', 2020, 'class002', 'persontype003', NULL);
INSERT INTO `person` VALUES ('stu003', '20200640128', 'zzf123', '张三丰', '男', '1999-07-23', '湖北省孝感市', '13822334456', 2020, 'class001', 'persontype003', NULL);
INSERT INTO `person` VALUES ('teacher001', '19950810101', '123', '熊正刚', '女', '2019-09-03', '广东省汕头市', '13718872231', 1995, NULL, 'persontype002', NULL);
INSERT INTO `person` VALUES ('teacher002', '19950810102', '123', '徐燕', '女', '1989-10-11', '河南省郑州市', '13713312231', 1994, NULL, 'persontype002', NULL);

-- ----------------------------
-- Table structure for person_type
-- ----------------------------
DROP TABLE IF EXISTS `person_type`;
CREATE TABLE `person_type`  (
  `id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `person_type_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of person_type
-- ----------------------------
INSERT INTO `person_type` VALUES ('persontype001', '管理员');
INSERT INTO `person_type` VALUES ('persontype002', '教师');
INSERT INTO `person_type` VALUES ('persontype003', '学生');

SET FOREIGN_KEY_CHECKS = 1;
